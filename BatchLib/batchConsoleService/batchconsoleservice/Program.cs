﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hangfire;
using Hangfire.SqlServer;
using log4net;
using System.Configuration;
using Newtonsoft;
using Newtonsoft.Json;

namespace EmailService
{
    class Program
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
           (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);



        static void Main(string[] args)
        {
            // EmailLib.EmailService.SendInternalEmail("Testing Email Crossbridge","This is the email send test");
            //GlobalConfiguration.Configuration.UseSqlServerStorage("data source=vs1sb3g4dbpr1m4.co8o5pwx7mvv.ap-southeast-1.rds.amazonaws.com; UID=root; PWD=password; initial catalog=riabadmin;");
          
            string HangFireDB = ConfigurationManager.AppSettings["HangFireDB"].ToString();

            string GenerateCatalogueHistoryPriceFilesSchedule = ConfigurationManager.AppSettings["GenerateCatalogueHistoryPriceFiles"].ToString();

            string UpdateCustomerProfileGoalsInvestmentValuesSchedule = ConfigurationManager.AppSettings["UpdateCustomerProfileGoalsInvestmentValues"].ToString();

            BatchLib.EmailService.SendInternalEmail("Test123", "Testing Mutiple Accounts");

            //BatchLib.EmailService.SendEmail(new BatchLib.EmailPayload());

            GlobalConfiguration.Configuration.UseSqlServerStorage(HangFireDB);
            
            using (var server = new BackgroundJobServer())
            {
                server.Start();
                Console.WriteLine("Email Service Server started. Press any key to exit...");
                log.Debug("Batch Process started at "+DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"));

                Console.WriteLine("Registering the Batch Jobs for Generate Hostory Price Files");

                RecurringJob.AddOrUpdate(() => BatchLib.CatalogueBatchService.GenerateCatalogueHistoryPriceFiles(), GenerateCatalogueHistoryPriceFilesSchedule, TimeZoneInfo.Local);
                RecurringJob.AddOrUpdate(() => BatchLib.CustomerBatchService.UpdateCustomerProfileGoalsInvestmentValues(), UpdateCustomerProfileGoalsInvestmentValuesSchedule, TimeZoneInfo.Local);

                BatchLib.CatalogueBatchService.GenerateCatalogueHistoryPriceFiles();
                Task<bool> b =BatchLib.CustomerBatchService.UpdateCustomerProfileGoalsInvestmentValues();



                //BatchLib.CatalogueBatchService.GenerateCatalogueHistoryPriceFiles();

                //BatchLib.CatalogueBatchService.GenerateCatalogueHistoryPriceFiles();

                //Task<bool> b = BatchLib.CustomerBatchService.UpdateCustomerProfileGoalsInvestmentValues();

                // RecurringJob.AddOrUpdate(() => BatchLib.CatalogueBatchService.GenerateCatalogueHistoryPriceFiles(), "04 14 * * *" , TimeZoneInfo.Local);

                // RecurringJob.AddOrUpdate(() => BatchLib.CustomerBatchService.UpdateCustomerProfileGoalsInvestmentValues(), "04 14 * * *", TimeZoneInfo.Local);

                Console.ReadKey();
            }
        }
     }
}