namespace BatchLib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Country
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        [StringLength(255)]
        public string CountryCode { get; set; }

        [StringLength(255)]
        public string CountryName { get; set; }

        [StringLength(255)]
        public string CurrencyCode { get; set; }

        [StringLength(255)]
        public string CurrencyName { get; set; }
    }
}
