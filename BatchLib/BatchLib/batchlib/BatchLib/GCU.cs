namespace BatchLib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("GCUS")]
    public partial class GCU
    {
        public long Id { get; set; }

        [StringLength(1)]
        public string CSIndicator { get; set; }

        [StringLength(50)]
        public string AccountNumber { get; set; }

        public decimal? BalanceCash { get; set; }

        [StringLength(100)]
        public string ModifiedBy { get; set; }

        public DateTime? Modified { get; set; }

        [StringLength(100)]
        public string CreatedBy { get; set; }

        public DateTime? Created { get; set; }
    }
}
