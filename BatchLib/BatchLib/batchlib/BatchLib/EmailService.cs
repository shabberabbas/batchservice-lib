using log4net;
using RazorEngine;
using RazorEngine.Templating;
using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Text.RegularExpressions;

namespace BatchLib
{
	public class EmailService
	{
		private static string sSource = "dotNET Sample App";

		private static string sLog = "Application";

		private static string sEvent = "Sample Event";

		private static string TermplateLocation = ConfigurationSettings.AppSettings["TemplateLocation"];

		private static string SmtpHost = ConfigurationSettings.AppSettings["SmtpHost"];

		private static string SmtpPort = ConfigurationSettings.AppSettings["SmtpPort"];

		private static string MailFrom = ConfigurationSettings.AppSettings["MailFrom"];

		private static string SmtpUser = ConfigurationSettings.AppSettings["SmtpUser"];

		private static string SmtpPass = ConfigurationSettings.AppSettings["SmtpPass"];

		private static string FromName = ConfigurationSettings.AppSettings["FromName"];

		private static string CrossbridgeNotificationEmail = ConfigurationSettings.AppSettings["CrossbridgeNotificationEmail"];

		public static string HangFireDB = ConfigurationSettings.AppSettings["HangFireDB"];

		private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		public static void SendInternalEmail(string Subject, string emailBody)
		{
			//IL_0154: Unknown result type (might be due to invalid IL or missing references)
			int num = 2;
			string smtpHost = EmailService.SmtpHost;
			string smtpPort = EmailService.SmtpPort;
			string smtpUser = EmailService.SmtpUser;
			string smtpPass = EmailService.SmtpPass;
			string crossbridgeNotificationEmail = EmailService.CrossbridgeNotificationEmail;
			string mailFrom = EmailService.MailFrom;
			string fromName = EmailService.FromName;
			SmtpClient smtpClient = new SmtpClient();
			MailMessage mailMessage = new MailMessage();
			MailAddress from = new MailAddress(mailFrom, fromName);
			string[] array = Regex.Split(crossbridgeNotificationEmail, ";");
			int port = int.Parse(smtpPort);
			smtpClient.Host = smtpHost;
			smtpClient.Port = port;
			smtpClient.EnableSsl = true;
			NetworkCredential networkCredential = (NetworkCredential)(smtpClient.Credentials = new NetworkCredential(smtpUser, smtpPass));
			mailMessage.From = from;
			mailMessage.Body = emailBody;
			mailMessage.IsBodyHtml = true;
			mailMessage.Subject = Subject;
			string[] array2 = array;
			foreach (string addresses in array2)
			{
				mailMessage.To.Add(addresses);
			}
			mailMessage.Priority = MailPriority.Normal;
			try
			{
				smtpClient.Send(mailMessage);
			}
			catch (Exception ex)
			{
				if (!EventLog.SourceExists(EmailService.sSource))
				{
					EventLog.CreateEventSource(EmailService.sSource, EmailService.sLog);
				}
				EventLog.WriteEntry(EmailService.sSource, ex.Message);
				EventLog.WriteEntry(EmailService.sSource, ex.InnerException.Message, EventLogEntryType.Warning, 234);
				EmailService.log.Error((object)(ex.Message + " InnerException:" + ex.InnerException.Message));
			}
		}

		public static void SendEmail(EmailPayload emailPayload)
		{
			//IL_02ae: Unknown result type (might be due to invalid IL or missing references)
			string emailSubject = emailPayload.EmailSubject;
			string emailBody = emailPayload.EmailBody;
			int num = 2;
			string smtpHost = EmailService.SmtpHost;
			string smtpPort = EmailService.SmtpPort;
			string smtpUser = EmailService.SmtpUser;
			string smtpPass = EmailService.SmtpPass;
			string toEmail = emailPayload.ToEmail;
			string mailFrom = EmailService.MailFrom;
			string fromName = EmailService.FromName;
			SmtpClient smtpClient = new SmtpClient();
			MailMessage mailMessage = new MailMessage();
			MailAddress from = new MailAddress(mailFrom, fromName);
			string[] array = Regex.Split(toEmail, ";");
			int port = int.Parse(smtpPort);
			smtpClient.Host = smtpHost;
			smtpClient.Port = port;
			smtpClient.EnableSsl = true;
			NetworkCredential networkCredential = (NetworkCredential)(smtpClient.Credentials = new NetworkCredential(smtpUser, smtpPass));
			mailMessage.From = from;
			if (array != null)
			{
				for (int i = 0; i < array.Length; i++)
				{
					if (array[i] != null && array[i] != "")
					{
						mailMessage.To.Add(array[i]);
					}
				}
			}
			switch (num)
			{
			case 1:
				mailMessage.Priority = MailPriority.High;
				break;
			case 3:
				mailMessage.Priority = MailPriority.Low;
				break;
			default:
				mailMessage.Priority = MailPriority.Normal;
				break;
			}
			mailMessage.Subject = emailSubject;
			mailMessage.IsBodyHtml = true;
			string text = File.ReadAllText(EmailService.TermplateLocation + "\\AccountCreated.html");
            string key = "email";
			switch (emailPayload.EmailType)
			{
			case "AccountCreated":
				text = File.ReadAllText(EmailService.TermplateLocation + "\\AccountCreated.html");
                key = "AccountCreated";
                break;
			case "AccountLocked":
				text = File.ReadAllText(EmailService.TermplateLocation + "\\AccountLocked.html");
                key = "AccountLocked";
                break;
			case "AccountReady":
				text = File.ReadAllText(EmailService.TermplateLocation + "\\AccountReady.html");
                key = "AccountReady";
                break;
			case "ConfirmPassword":
				text = File.ReadAllText(EmailService.TermplateLocation + "\\ConfirmPassword.html");
                key = "ConfirmPassword";
                break;
			case "ResetPassword":
				text = File.ReadAllText(EmailService.TermplateLocation + "\\ResetPassword.html");
                key = "ResetPassword";
                break;
			case "RedemptionRequest":
				text = File.ReadAllText(EmailService.TermplateLocation + "\\RedemptionRequest.html");
                key = "RedemptionRequest";
                break;
            case "AccountActive":
                text = File.ReadAllText(EmailService.TermplateLocation + "\\AccountActive.html");
                key = "AccountActive";
                break;
            default:
				text = null;
				break;
			}
            //Engine.Razor.RunCompile(template, "key", typeof(Person), model);
            mailMessage.Body = Engine.Razor.RunCompile(text, key, typeof(Customer1), emailPayload.customer);
            //mailMessage.Body = Engine.Razor..RunCompile(null,null,null, emailPayload.customer,null);
                
                //Engine.Razor.RunCompile(template, "templateKey", null, emailPayload.customer);
            
			try
			{
				if (text != null)
				{
					smtpClient.Send(mailMessage);
				}
			}
			catch (Exception ex)
			{
				if (!EventLog.SourceExists(EmailService.sSource))
				{
					EventLog.CreateEventSource(EmailService.sSource, EmailService.sLog);
				}
				EmailService.log.Error((object)(ex.Message + " InnerException:" + ex.InnerException.Message));
			}
		}
	}
}
