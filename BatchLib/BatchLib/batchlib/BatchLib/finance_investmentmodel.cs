namespace BatchLib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class finance_investmentmodel
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long id { get; set; }

        [StringLength(31)]
        public string dtype { get; set; }

        [StringLength(31)]
        public string tenant_id { get; set; }

        [StringLength(255)]
        public string name { get; set; }

        [StringLength(255)]
        public string status { get; set; }

        public int? updateversion { get; set; }

        [StringLength(255)]
        public string version { get; set; }

        public double? expectedreturn { get; set; }

        public double? expectedsharperatio { get; set; }

        public double? expectedstandarddeviation { get; set; }

        public long? productid { get; set; }
    }
}
