namespace BatchLib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("IntegrationAuditLog")]
    public partial class IntegrationAuditLog
    {
        public int Id { get; set; }

        public int? FileID { get; set; }

        [StringLength(50)]
        public string FileName { get; set; }

        [Column(TypeName = "date")]
        public DateTime? FileDate { get; set; }

        [StringLength(50)]
        public string TableName { get; set; }

        [StringLength(50)]
        public string ColumnName { get; set; }

        [StringLength(10)]
        public string PKColumnValue { get; set; }

        [StringLength(250)]
        public string OldValue { get; set; }

        [StringLength(250)]
        public string NewValue { get; set; }

        [StringLength(1)]
        public string Action { get; set; }

        [StringLength(500)]
        public string Comments { get; set; }

        [StringLength(20)]
        public string LoggedBy { get; set; }

        public DateTime? LoggedDate { get; set; }

        [StringLength(20)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public virtual IntegrationFileLog IntegrationFileLog { get; set; }
    }
}
