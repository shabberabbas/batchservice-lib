namespace BatchLib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CustomerDoc
    {
        public long id { get; set; }

        public long? CustomerId { get; set; }

        [StringLength(30)]
        public string Doctype { get; set; }

        [StringLength(120)]
        public string Filename { get; set; }

        public byte[] File { get; set; }

        public bool isVerified { get; set; }

        public DateTime Created { get; set; }

        public DateTime? Modified { get; set; }

        [StringLength(100)]
        public string CreatedBy { get; set; }

        [StringLength(100)]
        public string ModifiedBy { get; set; }

        public bool disable { get; set; }

        public virtual Customer Customer { get; set; }
    }
}
