namespace BatchLib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("GTDE")]
    public partial class GTDE
    {
        public long id { get; set; }

        [StringLength(30)]
        public string OrderRefNo { get; set; }

        [StringLength(30)]
        public string TradeRefNo { get; set; }

        [StringLength(20)]
        public string AccountNumber { get; set; }

        public long? TradeUnits { get; set; }

        [StringLength(1)]
        public string MarketCode { get; set; }

        [StringLength(10)]
        public string SettlementLocCode { get; set; }

        [StringLength(20)]
        public string PershingUserId { get; set; }

        public decimal? TradeNetAmount { get; set; }

        public decimal? PurchasePrice { get; set; }

        [Column(TypeName = "date")]
        public DateTime? HoldingDate { get; set; }

        [StringLength(1)]
        public string BuySell { get; set; }

        public long? GoalId { get; set; }

        [StringLength(100)]
        public string ModifiedBy { get; set; }

        public DateTime? Modified { get; set; }

        [StringLength(100)]
        public string CreatedBy { get; set; }

        public DateTime? Created { get; set; }
    }
}
