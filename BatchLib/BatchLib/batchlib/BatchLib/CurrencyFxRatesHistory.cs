namespace BatchLib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CurrencyFxRatesHistory")]
    public partial class CurrencyFxRatesHistory
    {
        public long? id { get; set; }

        [Key]
        public long originalId { get; set; }

        [StringLength(200)]
        public string Currency { get; set; }

        public decimal? USDtoCurrency { get; set; }

        public decimal? CurrencytoUSD { get; set; }

        [Column(TypeName = "date")]
        public DateTime CreatedDate { get; set; }
    }
}
