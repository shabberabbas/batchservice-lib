namespace BatchLib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ra_fundingaccountinfo
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long id { get; set; }

        [StringLength(31)]
        public string tenant_id { get; set; }

        [StringLength(255)]
        public string accountname { get; set; }

        [StringLength(255)]
        public string accountnumber { get; set; }

        [StringLength(255)]
        public string bankcode { get; set; }

        [StringLength(255)]
        public string bankname { get; set; }

        [StringLength(255)]
        public string branchcode { get; set; }

        [StringLength(255)]
        public string country { get; set; }

        [StringLength(255)]
        public string iban { get; set; }

        [StringLength(255)]
        public string notes { get; set; }

        [StringLength(255)]
        public string othersource { get; set; }

        [StringLength(255)]
        public string swiftcode { get; set; }

        public int? updateversion { get; set; }

        [StringLength(255)]
        public string username { get; set; }

        [Column(TypeName = "date")]
        public DateTime? accountinfoupdateddate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? accountopeneddate { get; set; }

        [StringLength(255)]
        public string accounttypecode { get; set; }

        [Column(TypeName = "date")]
        public DateTime? closedate { get; set; }

        [StringLength(255)]
        public string foreignbankaccountind { get; set; }

        [StringLength(3)]
        public string futuresatementcurrency { get; set; }

        [Column(TypeName = "date")]
        public DateTime? futurestatementcurrencyeffdate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? pendingclosedate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? reactivateddate { get; set; }

        [StringLength(3)]
        public string statementcurrency { get; set; }

        [StringLength(255)]
        public string status { get; set; }

        [Column(TypeName = "date")]
        public DateTime? reopeneddate { get; set; }
    }
}
