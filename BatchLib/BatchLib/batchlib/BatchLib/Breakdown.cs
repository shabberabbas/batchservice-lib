namespace BatchLib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Breakdown
    {
        public int Id { get; set; }

        public int? ParentAssetClassId { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        public decimal? Weight { get; set; }

        public int? Duration { get; set; }

        public int? RiskAppetite { get; set; }

        public decimal? CurrentValue { get; set; }

        public decimal? PeriodicalInvestment { get; set; }

        public decimal? InvestmentPeriodInMonth { get; set; }

        public virtual ParentAssetClass ParentAssetClass { get; set; }
    }
}
