namespace BatchLib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Goal
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Goal()
        {
            GoalHoldings = new HashSet<GoalHolding>();
            GoalHoldingsHistories = new HashSet<GoalHoldingsHistory>();
            TransactionLogs = new HashSet<TransactionLog>();
            TransactionLogHistories = new HashSet<TransactionLogHistory>();
        }

        public long id { get; set; }

        public long? CustomerId { get; set; }

        [StringLength(100)]
        public string GoalCategory { get; set; }

        public int? Risk { get; set; }

        public int? RiskProfileId { get; set; }

        public int? ProductId { get; set; }

        public decimal TargetAmount { get; set; }

        public decimal InitialInvestment { get; set; }

        public int Term { get; set; }

        public decimal? ProjectedOptimisticScenario { get; set; }

        public decimal? ProjectedMediumScenario { get; set; }

        public decimal? ProjectedPessimisticScenario { get; set; }

        [Required]
        [StringLength(100)]
        public string CreatedBy { get; set; }

        public DateTime? Created { get; set; }

        [StringLength(100)]
        public string ModifiedBy { get; set; }

        public DateTime? Modified { get; set; }

        public bool disable { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        public bool isRiskVerified { get; set; }

        public long? RefGoalId { get; set; }

        [StringLength(128)]
        public string userId { get; set; }

        public virtual Customer Customer { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GoalHolding> GoalHoldings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GoalHoldingsHistory> GoalHoldingsHistories { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TransactionLog> TransactionLogs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TransactionLogHistory> TransactionLogHistories { get; set; }
    }
}
