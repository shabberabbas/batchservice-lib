namespace BatchLib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class RiskProfile
    {
        public long id { get; set; }

        public string RiskLongDesc { get; set; }

        [StringLength(100)]
        public string RiskDesc { get; set; }

        public bool disable { get; set; }

        public DateTime? Created { get; set; }

        public DateTime? Modified { get; set; }

        [StringLength(100)]
        public string CreatedBy { get; set; }

        [StringLength(100)]
        public string ModifiedBy { get; set; }

        public int? Risk { get; set; }

        public long? CatalogueItemId { get; set; }

        [StringLength(30)]
        public string Currency { get; set; }
    }
}
