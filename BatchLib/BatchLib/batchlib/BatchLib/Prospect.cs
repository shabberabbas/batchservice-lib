namespace BatchLib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Prospect
    {
        [StringLength(100)]
        public string email { get; set; }

        public DateTime? Created { get; set; }

        [StringLength(30)]
        public string sourceip { get; set; }

        public int id { get; set; }
    }
}
