namespace BatchLib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class position_holdinggrouphistorypolicy
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long id { get; set; }

        [StringLength(31)]
        public string tenant_id { get; set; }

        public bool? canwrite { get; set; }

        public DateTime? creationdate { get; set; }

        public bool? owner { get; set; }

        [StringLength(255)]
        public string recipientid { get; set; }

        public long? sharedentityid { get; set; }

        [StringLength(255)]
        public string treeid { get; set; }

        public int? updateversion { get; set; }
    }
}
