namespace BatchLib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TransactionLogHistory")]
    public partial class TransactionLogHistory
    {
        public long? id { get; set; }

        [Key]
        public long OriginalId { get; set; }

        public decimal? Amount { get; set; }

        public long? TotalUnits { get; set; }

        public long? CustomerId { get; set; }

        public int? txType { get; set; }

        [StringLength(10)]
        public string txStatus { get; set; }

        [StringLength(20)]
        public string transactionRefNo { get; set; }

        public long? GoalId { get; set; }

        [StringLength(100)]
        public string CreatedBy { get; set; }

        [StringLength(100)]
        public string ModifiedBy { get; set; }

        public DateTime? Created { get; set; }

        public DateTime? Modified { get; set; }

        [StringLength(10)]
        public string Currency { get; set; }

        public bool disable { get; set; }

        public virtual Customer Customer { get; set; }

        public virtual Goal Goal { get; set; }
    }
}
