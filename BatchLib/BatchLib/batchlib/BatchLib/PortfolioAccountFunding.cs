namespace BatchLib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PortfolioAccountFunding")]
    public partial class PortfolioAccountFunding
    {
        public long id { get; set; }

        public long? CustomerId { get; set; }

        public long? PortfolioAccountId { get; set; }

        public decimal? Amount { get; set; }

        [StringLength(6)]
        public string Currency { get; set; }

        public DateTime? Created { get; set; }

        public DateTime? Modified { get; set; }

        [StringLength(100)]
        public string CreatedBy { get; set; }

        [StringLength(100)]
        public string ModifiedBy { get; set; }

        public bool disable { get; set; }

        public virtual CustomerPortfolioAccount CustomerPortfolioAccount { get; set; }

        public virtual Customer Customer { get; set; }
    }
}
