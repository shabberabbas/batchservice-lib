namespace BatchLib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CatItemHistoryPrice
    {
        public int id { get; set; }

        public int CatalogueItemId { get; set; }

        public decimal? LatestPrice { get; set; }

        public DateTime? LatestPriceDate { get; set; }

        [StringLength(6)]
        public string Currency { get; set; }

        public bool? disable { get; set; }

        public DateTime? Modified { get; set; }

        [StringLength(128)]
        public string ModifiedBy { get; set; }

        public DateTime? Created { get; set; }

        public virtual CatalogueItem CatalogueItem { get; set; }
    }
}
