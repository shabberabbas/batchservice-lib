namespace BatchLib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("IntegrationACCTFILE")]
    public partial class IntegrationACCTFILE
    {
        public int Id { get; set; }

        public int? FileID { get; set; }

        [StringLength(50)]
        public string FileName { get; set; }

        public DateTime? FileDate { get; set; }

        [StringLength(10)]
        public string RecordCount { get; set; }

        public int? FileSequenceNumber { get; set; }

        public int? CustomerId { get; set; }

        [StringLength(100)]
        public string TransactionType { get; set; }

        [StringLength(100)]
        public string RecordType { get; set; }

        [StringLength(1)]
        public string AType { get; set; }

        [StringLength(1)]
        public string BType { get; set; }

        [StringLength(1)]
        public string CType { get; set; }

        [StringLength(1)]
        public string WType { get; set; }

        [StringLength(1)]
        public string FourType { get; set; }

        [StringLength(1)]
        public string GType { get; set; }

        [StringLength(1)]
        public string HType { get; set; }

        [StringLength(1)]
        public string XType { get; set; }

        [StringLength(1)]
        public string MType { get; set; }

        [StringLength(1)]
        public string IType { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(100)]
        public string AccountNumber { get; set; }

        [StringLength(100)]
        public string Nationality { get; set; }

        public DateTime? BirthDate { get; set; }

        [StringLength(100)]
        public string Status { get; set; }

        [StringLength(100)]
        public string Address { get; set; }

        [StringLength(100)]
        public string City { get; set; }

        [StringLength(100)]
        public string State { get; set; }

        [StringLength(100)]
        public string CountryCode { get; set; }

        [StringLength(100)]
        public string PostalCode { get; set; }

        [StringLength(100)]
        public string PassportNumber { get; set; }

        [StringLength(100)]
        public string PassportIssuingCountry { get; set; }

        [StringLength(100)]
        public string PassportExpiredDate { get; set; }

        [StringLength(100)]
        public string PassportIssuingDate { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        [StringLength(100)]
        public string TaxResidence { get; set; }

        public DateTime? RecordDate { get; set; }

        public virtual IntegrationFileLog IntegrationFileLog { get; set; }
    }
}
