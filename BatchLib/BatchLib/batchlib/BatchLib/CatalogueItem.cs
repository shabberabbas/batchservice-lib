namespace BatchLib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CatalogueItem")]
    public class CatalogueItem
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CatalogueItem()
        {
            CatItemHistoryPrices = new HashSet<CatItemHistoryPrice>();
            ProjectionDatas = new HashSet<ProjectionData>();
        }

        public int id { get; set; }

        public int? ProductTypeId { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(30)]
        public string BusinessCode { get; set; }

        [StringLength(30)]
        public string ISIN { get; set; }

        [StringLength(16)]
        public string Ticker { get; set; }

        [StringLength(4)]
        public string Registration { get; set; }

        [StringLength(4)]
        public string Listing { get; set; }

        public int? LotSize { get; set; }

        [StringLength(30)]
        public string Currency { get; set; }

        public decimal? Settlement { get; set; }

        public decimal? AskPrice { get; set; }

        public decimal? BidPrice { get; set; }

        public decimal? FaceValue { get; set; }

        public decimal? Price { get; set; }

        public decimal? Score { get; set; }

        public decimal? Value { get; set; }

        public DateTime? Modified { get; set; }

        [StringLength(100)]
        public string ModifiedBy { get; set; }

        public DateTime? PreviousPriceDate { get; set; }

        public decimal? PreviousPrice { get; set; }

        public decimal? LatestPrice { get; set; }

        public DateTime? LatestPriceDate { get; set; }

        public DateTime? Created { get; set; }

        [StringLength(100)]
        public string CreatedBy { get; set; }

        public bool? disable { get; set; }

        public decimal? ExpectedReturn { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CatItemHistoryPrice> CatItemHistoryPrices { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProjectionData> ProjectionDatas { get; set; }
    }
}
