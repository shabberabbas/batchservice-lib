namespace BatchLib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CustomerStatu
    {
        public int id { get; set; }

        [StringLength(60)]
        public string Status { get; set; }

        [StringLength(60)]
        public string StatusText { get; set; }

        public DateTime? Created { get; set; }

        [StringLength(100)]
        public string CreatedBy { get; set; }

        public DateTime? Modified { get; set; }

        [StringLength(100)]
        public string ModifiedBy { get; set; }

        public bool disable { get; set; }
    }
}
