namespace BatchLib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CustomerPortfolioAccountHistory")]
    public partial class CustomerPortfolioAccountHistory
    {
        public long? id { get; set; }

        public long? CustomerId { get; set; }

        public decimal? balanceCash { get; set; }

        public decimal? LatestInvestment { get; set; }

        public decimal? InitialInvestment { get; set; }

        public decimal? investedCash { get; set; }

        public decimal? investmentValue { get; set; }

        [StringLength(6)]
        public string Currency { get; set; }

        [StringLength(50)]
        public string AccountNumber { get; set; }

        public DateTime? Created { get; set; }

        public DateTime? Modified { get; set; }

        [StringLength(100)]
        public string CreatedBy { get; set; }

        [StringLength(100)]
        public string ModifiedBy { get; set; }

        public bool? disable { get; set; }

        [Key]
        public long Originalid { get; set; }

        public virtual Customer Customer { get; set; }
    }
}
