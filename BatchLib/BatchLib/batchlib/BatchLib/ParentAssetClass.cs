namespace BatchLib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ParentAssetClass
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ParentAssetClass()
        {
            Breakdowns = new HashSet<Breakdown>();
        }

        public int Id { get; set; }

        public int? ParentAssetGroupId { get; set; }

        [StringLength(50)]
        public string ParrentAssetClass { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Breakdown> Breakdowns { get; set; }

        public virtual ParentAssetGroup ParentAssetGroup { get; set; }
    }
}
