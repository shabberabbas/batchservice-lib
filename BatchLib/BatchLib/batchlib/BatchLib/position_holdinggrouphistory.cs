namespace BatchLib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class position_holdinggrouphistory
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long id { get; set; }

        [StringLength(31)]
        public string tenant_id { get; set; }

        [StringLength(255)]
        public string name { get; set; }

        public int? updateversion { get; set; }

        [Column(TypeName = "date")]
        public DateTime? value_basecurrencyvaluedate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? value_refcurrencyvaluedate { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? value_basecurrencyvalue_amount { get; set; }

        [StringLength(3)]
        public string value_basecurrencyvalue_currency { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? value_refcurrencyvalue_amount { get; set; }

        [StringLength(3)]
        public string value_refcurrencyvalue_currency { get; set; }

        public DateTime? createdon { get; set; }

        [Column(TypeName = "date")]
        public DateTime? investedcash_basecurrencyvaluedate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? investedcash_refcurrencyvaluedate { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? investedcash_basecurrencyvalue_amount { get; set; }

        [StringLength(3)]
        public string investedcash_basecurrencyvalue_currency { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? investedcash_refcurrencyvalue_amount { get; set; }

        [StringLength(3)]
        public string investedcash_refcurrencyvalue_currency { get; set; }
    }
}
