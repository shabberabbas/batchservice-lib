namespace BatchLib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("IntegrationRecordLog")]
    public partial class IntegrationRecordLog
    {
        public int Id { get; set; }

        public int? FileID { get; set; }

        [StringLength(50)]
        public string FileName { get; set; }

        public DateTime? FileDate { get; set; }

        public int? RecordNumber { get; set; }

        public int? FileSequenceNumber { get; set; }

        public int? CustomerId { get; set; }

        [StringLength(1)]
        public string Action { get; set; }

        [StringLength(20)]
        public string Result { get; set; }

        [StringLength(500)]
        public string Comments { get; set; }

        [StringLength(500)]
        public string Error { get; set; }

        public DateTime? LogDate { get; set; }

        public virtual IntegrationFileLog IntegrationFileLog { get; set; }
    }
}
