namespace BatchLib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Customer
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Customer()
        {
            CustomerDocs = new HashSet<CustomerDoc>();
            CustomerPortfolioAccounts = new HashSet<CustomerPortfolioAccount>();
            CustomerPortfolioAccountDailies = new HashSet<CustomerPortfolioAccountDaily>();
            CustomerPortfolioAccountHistories = new HashSet<CustomerPortfolioAccountHistory>();
            Goals = new HashSet<Goal>();
            PortfolioAccountFundings = new HashSet<PortfolioAccountFunding>();
            TransactionLogs = new HashSet<TransactionLog>();
            TransactionLogHistories = new HashSet<TransactionLogHistory>();
        }

        public long id { get; set; }

        [StringLength(100)]
        public string email { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(15)]
        public string Phone { get; set; }

        [StringLength(15)]
        public string Mobile { get; set; }

        [StringLength(15)]
        public string OfficePhone { get; set; }

        [StringLength(250)]
        public string Address { get; set; }

        [StringLength(20)]
        public string City { get; set; }

        [StringLength(20)]
        public string State { get; set; }

        [StringLength(25)]
        public string Country { get; set; }

        [StringLength(60)]
        public string Status { get; set; }

        public DateTime? Created { get; set; }

        [StringLength(100)]
        public string ModifiedBy { get; set; }

        public DateTime? Modified { get; set; }

        public bool disable { get; set; }

        public Guid? ActivationCode { get; set; }

        public string Password { get; set; }

        [StringLength(50)]
        public string Alias { get; set; }

        [StringLength(100)]
        public string MothersMaidenName { get; set; }

        [Column(TypeName = "date")]
        public DateTime? birthDate { get; set; }

        [StringLength(100)]
        public string Citizenship { get; set; }

        [StringLength(100)]
        public string TaxResidency { get; set; }

        public string SourceOfFundingOptions { get; set; }

        public string BankDetails { get; set; }

        public string LocalBankDetails { get; set; }

        [StringLength(20)]
        public string PassportNo { get; set; }

        [Column(TypeName = "date")]
        public DateTime? PassportIssueDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? PassportExpiryDate { get; set; }

        [StringLength(100)]
        public string PassportIssueCountry { get; set; }

        public bool? LockOutEmailSentFlag { get; set; }

        [StringLength(100)]
        public string CreatedBy { get; set; }

        [StringLength(30)]
        public string NRIC { get; set; }

        [StringLength(20)]
        public string PostalCode { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomerDoc> CustomerDocs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomerPortfolioAccount> CustomerPortfolioAccounts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomerPortfolioAccountDaily> CustomerPortfolioAccountDailies { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomerPortfolioAccountHistory> CustomerPortfolioAccountHistories { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Goal> Goals { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PortfolioAccountFunding> PortfolioAccountFundings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TransactionLog> TransactionLogs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TransactionLogHistory> TransactionLogHistories { get; set; }
    }
}
