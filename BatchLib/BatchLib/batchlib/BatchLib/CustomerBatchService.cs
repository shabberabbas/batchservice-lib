using log4net;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace BatchLib
{
	public class CustomerBatchService
	{
		private static ConnectEntities db = new ConnectEntities();

		private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		public static bool UpdateCurrentValue()
		{
			return true;
		}

		public static async Task<bool> UpdateCustomerProfileGoalsInvestmentValues()
		{
            

            int a = 0;

            //a = a + 9;

            int bt = a;


            // temp comment
            var goalsHoldings = (from c in db.GoalHoldings
                                 from g in db.Goals.Where(map => map.id == c.GoalId)
                                 from b in db.RiskProfiles.Where(map => map.Risk == g.RiskProfileId)
                                 from p in db.CatalogueItems.Where(map => map.id == b.CatalogueItemId).DefaultIfEmpty()
                                 select new { id = c.id, CustomerId = g.CustomerId, CurrentHoldingUnits = c.CurrentHoldingUnits, LatestPrice = p.LatestPrice }
                ).ToList();

          

            bt = a * 8;

            bool dbChanges = false;
            //var goalsHoldings = goalsHoldingsQuery.ToList();
            for (int k = 0; k < goalsHoldings.Count; k++)
            {
                GoalHolding g2 = (GoalHolding)CustomerBatchService.db.GoalHoldings.Find(goalsHoldings[k].id);
                GoalHoldingsHistory gh = new GoalHoldingsHistory();
                CustomerBatchService.db.GoalHoldingsHistories.Add(gh);
                CustomerBatchService.db.Entry(gh).CurrentValues.SetValues(g2);
                g2.Modified = DateTime.Now;
                g2.ModifiedBy = "UpdateCurrentValueProcess";
                g2.InvestmentValue = goalsHoldings[k].LatestPrice * (decimal?)goalsHoldings[k].CurrentHoldingUnits;
                //CustomerBatchService.db.Entry<GoalHolding>(g2).set_State(16);

                CustomerBatchService.db.Entry(g2).State = EntityState.Modified;

                dbChanges = true;
            }
            CustomerPortfolioAccount cp = new CustomerPortfolioAccount();
            IEnumerable<long?> custIds = (from x in goalsHoldings
                                          select x.CustomerId).Distinct();
            string emailHTMLBody6 = "<table border=1>";
            emailHTMLBody6 += "<tr><th>Email</th><th>Total Investment Value</th><th>Total Available Cash</th><th>Goals</th></tr>";
            for (int j = 0; j < custIds.Count(); j++)
            {
                long? custId = custIds.ElementAt(j);
                decimal totalValue = (from x in goalsHoldings
                                      where x.CustomerId == custId && x.CurrentHoldingUnits.HasValue && x.LatestPrice.HasValue
                                      select x into c
                                      select (decimal)c.CurrentHoldingUnits.Value * c.LatestPrice.Value).Sum();
                CustomerPortfolioAccount custport = await (from c in CustomerBatchService.db.CustomerPortfolioAccounts where c.CustomerId.Value == custId.Value
                                                                                                                            select c).FirstOrDefaultAsync<CustomerPortfolioAccount>();

                cp = custport;
                if (custport != null)
                {
                    CustomerPortfolioAccountHistory custAcctPortfolioHis = new CustomerPortfolioAccountHistory();
                    CustomerBatchService.db.CustomerPortfolioAccountHistories.Add(custAcctPortfolioHis);
                    CustomerBatchService.db.Entry(custAcctPortfolioHis).CurrentValues.SetValues(cp);
                    custport.Modified = DateTime.Now;
                    custport.ModifiedBy = "UpdateCurrentValueProcess";
                    custport.investmentValue = totalValue;
                    dbChanges = true;
                    //}
                    Customer customerRec = (Customer)CustomerBatchService.db.Customers.Find(custId);
                    emailHTMLBody6 = emailHTMLBody6 + "<tr><td>" + customerRec.email + "</td><td>" + custport.investmentValue + "</td><td>" + custport.balanceCash + "</td><td>";
                    emailHTMLBody6 += "<table border=1>";
                    emailHTMLBody6 += "<tr><th>GoalId</th><th>Quantity</th><th>LatestPrice</th></tr>";
                    var cGoals = (from x in goalsHoldings
                                  where x.CustomerId == custId
                                  select x).ToList();
                    for (int i = 0; i < cGoals.Count(); i++)
                    {
                        var cGoal = cGoals.ElementAt(i);
                        emailHTMLBody6 = emailHTMLBody6 + "<tr><td>" + cGoal.id + "</td><td>" + cGoal.CurrentHoldingUnits + "</td><td>" + cGoal.LatestPrice + "</td></tr>";
                    }
                    emailHTMLBody6 += "</table></td></tr>";
                }
            }
            emailHTMLBody6 += "</table>";
            object[] obj5 = new object[4]
            {
                "Last Value Update Process runs at ",
                null,
                null,
                null
            };
            DateTime now = DateTime.Now;
            obj5[1] = now.ToString("yyyy MMMM dd HH:mm:ss");
            obj5[2] = " Total Number of records processed ";
            obj5[3] = custIds.Count();
            string ActionText = string.Concat(obj5);
            if (dbChanges)
            {
                try
                {
                    if (await CustomerBatchService.db.SaveChangesAsync() > 0)
                    {
                        await RecordAuditEntry.RecordEntry(0L, 0L, ActionText, "localhost", "UpdateCurrentValueProcess");
                        now = DateTime.Now;
                        EmailService.SendInternalEmail("CONNECT Update Investment Value Process", "Process Runs at " + now.ToString("dd-MM-yyyy HH:MM:ss") + "<br><br>" + emailHTMLBody6);
                        return true;
                    }
                }
                catch (Exception ets)
                {
                    CustomerBatchService.log.Error((object)"Error in Update Customer Account Update");
                    if (ets != null)
                    {
                        CustomerBatchService.log.Error((object)ets.Message);
                    }
                    if (ets.InnerException != null)
                    {
                        CustomerBatchService.log.Error((object)ets.InnerException.Message);
                    }
                }

                // bool saveFailed; 
                //do 
                //{ 
                //    saveFailed = false; 

                //    try
                //    {
                //        if (await CustomerBatchService.db.SaveChangesAsync() > 0)
                //        {
                //            await RecordAuditEntry.RecordEntry(0L, 0L, ActionText, "localhost", "UpdateCurrentValueProcess");
                //            now = DateTime.Now;
                //            EmailService.SendInternalEmail("CONNECT Update Investment Value Process", "Process Runs at " + now.ToString("dd-MM-yyyy HH:MM:ss") + "<br><br>" + emailHTMLBody6);
                //            return true;
                //        }
                //    }
                //   catch (DbUpdateConcurrencyException ex) 
                //    { 
                //        saveFailed = true; 
 
                //        // Update the values of the entity that failed to save from the store 
                //        ex.Entries.Single().Reload();
                //        saveFailed = saveFailed;
                //    }

                //} while (saveFailed); 


                return false;
            }
			return false;
		}
	}
}
