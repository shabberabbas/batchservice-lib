namespace BatchLib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ACCTHistory")]
    public partial class ACCTHistory
    {
        public long? Id { get; set; }

        public int? CustomerId { get; set; }

        [StringLength(3)]
        public string AccountType { get; set; }

        [StringLength(50)]
        public string AccountNumber { get; set; }

        [StringLength(50)]
        public string Citizenship { get; set; }

        public DateTime? birthDate { get; set; }

        [StringLength(50)]
        public string TaxResidency { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        [StringLength(50)]
        public string FirstName1 { get; set; }

        [StringLength(50)]
        public string FirstName2 { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(50)]
        public string IdentityType { get; set; }

        [StringLength(50)]
        public string PassportNo { get; set; }

        [StringLength(50)]
        public string PassportIssueCountry { get; set; }

        public DateTime? PassportExpiryDate { get; set; }

        [StringLength(50)]
        public string PassportIssueDate { get; set; }

        [StringLength(50)]
        public string email { get; set; }

        [StringLength(50)]
        public string Country { get; set; }

        [StringLength(50)]
        public string Address1 { get; set; }

        [StringLength(50)]
        public string Address2 { get; set; }

        [StringLength(50)]
        public string Address3 { get; set; }

        [StringLength(50)]
        public string Address4 { get; set; }

        [StringLength(50)]
        public string USCity { get; set; }

        [StringLength(50)]
        public string NonUSCity { get; set; }

        [StringLength(50)]
        public string USState { get; set; }

        [StringLength(50)]
        public string USPostalCode { get; set; }

        [Key]
        public long OriginalId { get; set; }

        [StringLength(100)]
        public string ModifiedBy { get; set; }

        public DateTime? Modified { get; set; }

        [StringLength(100)]
        public string CreatedBy { get; set; }

        public DateTime? Created { get; set; }
    }
}
