namespace BatchLib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class catalogue_catalogueitemhistory
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long id { get; set; }

        [StringLength(31)]
        public string tenant_id { get; set; }

        [StringLength(255)]
        public string createdby { get; set; }

        public DateTime? createddate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? latestpricedate { get; set; }

        [StringLength(255)]
        public string modifiedby { get; set; }

        public DateTime? modifieddate { get; set; }

        [StringLength(255)]
        public string name { get; set; }

        [StringLength(255)]
        public string pershingdescription { get; set; }

        public int? updateversion { get; set; }

        [Column(TypeName = "date")]
        public DateTime? askprice_basecurrencyvaluedate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? askprice_refcurrencyvaluedate { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? askprice_basecurrencyvalue_amount { get; set; }

        [StringLength(3)]
        public string askprice_basecurrencyvalue_currency { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? askprice_refcurrencyvalue_amount { get; set; }

        [StringLength(3)]
        public string askprice_refcurrencyvalue_currency { get; set; }

        [Column(TypeName = "date")]
        public DateTime? bidprice_basecurrencyvaluedate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? bidprice_refcurrencyvaluedate { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? bidprice_basecurrencyvalue_amount { get; set; }

        [StringLength(3)]
        public string bidprice_basecurrencyvalue_currency { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? bidprice_refcurrencyvalue_amount { get; set; }

        [StringLength(3)]
        public string bidprice_refcurrencyvalue_currency { get; set; }

        [Column(TypeName = "date")]
        public DateTime? latestprice_basecurrencyvaluedate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? latestprice_refcurrencyvaluedate { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? latestprice_basecurrencyvalue_amount { get; set; }

        [StringLength(3)]
        public string latestprice_basecurrencyvalue_currency { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? latestprice_refcurrencyvalue_amount { get; set; }

        [StringLength(3)]
        public string latestprice_refcurrencyvalue_currency { get; set; }

        [Column(TypeName = "date")]
        public DateTime? value_basecurrencyvaluedate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? value_refcurrencyvaluedate { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? value_basecurrencyvalue_amount { get; set; }

        [StringLength(3)]
        public string value_basecurrencyvalue_currency { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? value_refcurrencyvalue_amount { get; set; }

        [StringLength(3)]
        public string value_refcurrencyvalue_currency { get; set; }

        [StringLength(255)]
        public string businesscode { get; set; }

        [StringLength(255)]
        public string secondarybusinesscode { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? price_amount { get; set; }

        [StringLength(3)]
        public string price_currency { get; set; }

        public long? catalogueitemid { get; set; }
    }
}
