namespace BatchLib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class LockedUser
    {
        public long id { get; set; }

        [StringLength(200)]
        public string UserName { get; set; }

        public DateTime? LockedDate { get; set; }

        public bool disable { get; set; }
    }
}
