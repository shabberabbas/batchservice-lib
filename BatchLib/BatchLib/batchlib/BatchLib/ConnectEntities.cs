namespace BatchLib
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ConnectEntities : DbContext
    {
        public ConnectEntities()
            : base("name=ConnectEntities")
        {
        }

        public virtual DbSet<AspNetRole> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaim> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogin> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUser> AspNetUsers { get; set; }
        public virtual DbSet<Breakdown> Breakdowns { get; set; }
        public virtual DbSet<catalogue_catalogueitemhistory> catalogue_catalogueitemhistory { get; set; }
        public virtual DbSet<CatalogueItem> CatalogueItems { get; set; }
        public virtual DbSet<CatalogueItemHistory> CatalogueItemHistories { get; set; }
        public virtual DbSet<CatItemHistoryPrice> CatItemHistoryPrices { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<CurrencyFxRate> CurrencyFxRates { get; set; }
        public virtual DbSet<CurrencyFxRatesHistory> CurrencyFxRatesHistories { get; set; }
        public virtual DbSet<CustomerDoc> CustomerDocs { get; set; }
        public virtual DbSet<CustomerPortfolioAccount> CustomerPortfolioAccounts { get; set; }
        public virtual DbSet<CustomerPortfolioAccountDaily> CustomerPortfolioAccountDailies { get; set; }
        public virtual DbSet<CustomerPortfolioAccountHistory> CustomerPortfolioAccountHistories { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<CustomerStatu> CustomerStatus { get; set; }
        public virtual DbSet<finance_investmentmodel> finance_investmentmodel { get; set; }
        public virtual DbSet<GoalHolding> GoalHoldings { get; set; }
        public virtual DbSet<GoalHoldingsHistory> GoalHoldingsHistories { get; set; }
        public virtual DbSet<Goal> Goals { get; set; }
        public virtual DbSet<GTDE> GTDEs { get; set; }
        public virtual DbSet<GTDEHistory> GTDEHistories { get; set; }
        public virtual DbSet<IntegrationFileLog> IntegrationFileLogs { get; set; }
        public virtual DbSet<IntegrationRecordLog> IntegrationRecordLogs { get; set; }
        public virtual DbSet<LockedUser> LockedUsers { get; set; }
        public virtual DbSet<ParentAssetClass> ParentAssetClasses { get; set; }
        public virtual DbSet<ParentAssetGroup> ParentAssetGroups { get; set; }
        public virtual DbSet<PortfolioAccountFunding> PortfolioAccountFundings { get; set; }
        public virtual DbSet<position_holdinggrouphistory> position_holdinggrouphistory { get; set; }
        public virtual DbSet<position_holdinggrouphistorypolicy> position_holdinggrouphistorypolicy { get; set; }
        public virtual DbSet<position_holdinghistory> position_holdinghistory { get; set; }
        public virtual DbSet<position_holdinghistory_groups> position_holdinghistory_groups { get; set; }
        public virtual DbSet<ProjectionData> ProjectionDatas { get; set; }
        public virtual DbSet<Prospect> Prospects { get; set; }
        public virtual DbSet<ra_fundingaccountinfo> ra_fundingaccountinfo { get; set; }
        public virtual DbSet<RiskProfile> RiskProfiles { get; set; }
        public virtual DbSet<TransactionLog> TransactionLogs { get; set; }
        public virtual DbSet<TransactionLogHistory> TransactionLogHistories { get; set; }
        public virtual DbSet<ACCT> ACCTs { get; set; }
        public virtual DbSet<ACCTHistory> ACCTHistories { get; set; }
        public virtual DbSet<CustomersHistory> CustomersHistories { get; set; }
        public virtual DbSet<GCU> GCUS { get; set; }
        public virtual DbSet<IntegrationACCTFILE> IntegrationACCTFILEs { get; set; }
        public virtual DbSet<IntegrationAuditLog> IntegrationAuditLogs { get; set; }
        public virtual DbSet<test> tests { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CatalogueItem>().ToTable("CatalogueItem");
            modelBuilder.Entity<AspNetRole>()
                .HasMany(e => e.AspNetUsers)
                .WithMany(e => e.AspNetRoles)
                .Map(m => m.ToTable("AspNetUserRoles").MapLeftKey("RoleId").MapRightKey("UserId"));

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.AspNetUserClaims)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.AspNetUserLogins)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<Breakdown>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<catalogue_catalogueitemhistory>()
                .Property(e => e.tenant_id)
                .IsUnicode(false);

            modelBuilder.Entity<catalogue_catalogueitemhistory>()
                .Property(e => e.createdby)
                .IsUnicode(false);

            modelBuilder.Entity<catalogue_catalogueitemhistory>()
                .Property(e => e.modifiedby)
                .IsUnicode(false);

            modelBuilder.Entity<catalogue_catalogueitemhistory>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<catalogue_catalogueitemhistory>()
                .Property(e => e.pershingdescription)
                .IsUnicode(false);

            modelBuilder.Entity<catalogue_catalogueitemhistory>()
                .Property(e => e.askprice_basecurrencyvalue_amount)
                .HasPrecision(19, 3);

            modelBuilder.Entity<catalogue_catalogueitemhistory>()
                .Property(e => e.askprice_basecurrencyvalue_currency)
                .IsUnicode(false);

            modelBuilder.Entity<catalogue_catalogueitemhistory>()
                .Property(e => e.askprice_refcurrencyvalue_amount)
                .HasPrecision(19, 3);

            modelBuilder.Entity<catalogue_catalogueitemhistory>()
                .Property(e => e.askprice_refcurrencyvalue_currency)
                .IsUnicode(false);

            modelBuilder.Entity<catalogue_catalogueitemhistory>()
                .Property(e => e.bidprice_basecurrencyvalue_amount)
                .HasPrecision(19, 3);

            modelBuilder.Entity<catalogue_catalogueitemhistory>()
                .Property(e => e.bidprice_basecurrencyvalue_currency)
                .IsUnicode(false);

            modelBuilder.Entity<catalogue_catalogueitemhistory>()
                .Property(e => e.bidprice_refcurrencyvalue_amount)
                .HasPrecision(19, 3);

            modelBuilder.Entity<catalogue_catalogueitemhistory>()
                .Property(e => e.bidprice_refcurrencyvalue_currency)
                .IsUnicode(false);

            modelBuilder.Entity<catalogue_catalogueitemhistory>()
                .Property(e => e.latestprice_basecurrencyvalue_amount)
                .HasPrecision(19, 3);

            modelBuilder.Entity<catalogue_catalogueitemhistory>()
                .Property(e => e.latestprice_basecurrencyvalue_currency)
                .IsUnicode(false);

            modelBuilder.Entity<catalogue_catalogueitemhistory>()
                .Property(e => e.latestprice_refcurrencyvalue_amount)
                .HasPrecision(19, 3);

            modelBuilder.Entity<catalogue_catalogueitemhistory>()
                .Property(e => e.latestprice_refcurrencyvalue_currency)
                .IsUnicode(false);

            modelBuilder.Entity<catalogue_catalogueitemhistory>()
                .Property(e => e.value_basecurrencyvalue_amount)
                .HasPrecision(19, 3);

            modelBuilder.Entity<catalogue_catalogueitemhistory>()
                .Property(e => e.value_basecurrencyvalue_currency)
                .IsUnicode(false);

            modelBuilder.Entity<catalogue_catalogueitemhistory>()
                .Property(e => e.value_refcurrencyvalue_amount)
                .HasPrecision(19, 3);

            modelBuilder.Entity<catalogue_catalogueitemhistory>()
                .Property(e => e.value_refcurrencyvalue_currency)
                .IsUnicode(false);

            modelBuilder.Entity<catalogue_catalogueitemhistory>()
                .Property(e => e.businesscode)
                .IsUnicode(false);

            modelBuilder.Entity<catalogue_catalogueitemhistory>()
                .Property(e => e.secondarybusinesscode)
                .IsUnicode(false);

            modelBuilder.Entity<catalogue_catalogueitemhistory>()
                .Property(e => e.price_amount)
                .HasPrecision(19, 3);

            modelBuilder.Entity<catalogue_catalogueitemhistory>()
                .Property(e => e.price_currency)
                .IsUnicode(false);

            modelBuilder.Entity<CatalogueItem>()
                .Property(e => e.Ticker)
                .IsFixedLength();

            modelBuilder.Entity<CatalogueItem>()
                .Property(e => e.Registration)
                .IsFixedLength();

            modelBuilder.Entity<CatalogueItem>()
                .Property(e => e.Listing)
                .IsFixedLength();

            modelBuilder.Entity<CatalogueItem>()
                .Property(e => e.Currency)
                .IsFixedLength();

            modelBuilder.Entity<CatalogueItem>()
                .HasMany(e => e.CatItemHistoryPrices)
                .WithRequired(e => e.CatalogueItem)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CatalogueItemHistory>()
                .Property(e => e.Ticker)
                .IsFixedLength();

            modelBuilder.Entity<CatalogueItemHistory>()
                .Property(e => e.Registration)
                .IsFixedLength();

            modelBuilder.Entity<CatalogueItemHistory>()
                .Property(e => e.Listing)
                .IsFixedLength();

            modelBuilder.Entity<CatalogueItemHistory>()
                .Property(e => e.Currency)
                .IsFixedLength();

            modelBuilder.Entity<CatItemHistoryPrice>()
                .Property(e => e.Currency)
                .IsFixedLength();

            modelBuilder.Entity<Country>()
                .Property(e => e.CountryCode)
                .IsUnicode(false);

            modelBuilder.Entity<Country>()
                .Property(e => e.CountryName)
                .IsUnicode(false);

            modelBuilder.Entity<Country>()
                .Property(e => e.CurrencyCode)
                .IsUnicode(false);

            modelBuilder.Entity<Country>()
                .Property(e => e.CurrencyName)
                .IsUnicode(false);

            modelBuilder.Entity<CurrencyFxRate>()
                .Property(e => e.Currency)
                .IsUnicode(false);

            modelBuilder.Entity<CurrencyFxRate>()
                .Property(e => e.USDtoCurrency)
                .HasPrecision(18, 5);

            modelBuilder.Entity<CurrencyFxRate>()
                .Property(e => e.CurrencytoUSD)
                .HasPrecision(18, 5);

            modelBuilder.Entity<CurrencyFxRatesHistory>()
                .Property(e => e.Currency)
                .IsUnicode(false);

            modelBuilder.Entity<CurrencyFxRatesHistory>()
                .Property(e => e.USDtoCurrency)
                .HasPrecision(18, 5);

            modelBuilder.Entity<CurrencyFxRatesHistory>()
                .Property(e => e.CurrencytoUSD)
                .HasPrecision(18, 5);

            modelBuilder.Entity<CustomerPortfolioAccount>()
                .Property(e => e.Currency)
                .IsFixedLength();

            modelBuilder.Entity<CustomerPortfolioAccount>()
                .HasMany(e => e.PortfolioAccountFundings)
                .WithOptional(e => e.CustomerPortfolioAccount)
                .HasForeignKey(e => e.PortfolioAccountId);

            modelBuilder.Entity<CustomerPortfolioAccountDaily>()
                .Property(e => e.Currency)
                .IsFixedLength();

            modelBuilder.Entity<CustomerPortfolioAccountHistory>()
                .Property(e => e.Currency)
                .IsFixedLength();

            modelBuilder.Entity<Customer>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.Mobile)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.OfficePhone)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.Address)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.State)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.Country)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.Citizenship)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.TaxResidency)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.PassportNo)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.PassportIssueCountry)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.NRIC)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.PostalCode)
                .IsUnicode(false);

            modelBuilder.Entity<CustomerStatu>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<CustomerStatu>()
                .Property(e => e.StatusText)
                .IsUnicode(false);

            modelBuilder.Entity<finance_investmentmodel>()
                .Property(e => e.dtype)
                .IsUnicode(false);

            modelBuilder.Entity<finance_investmentmodel>()
                .Property(e => e.tenant_id)
                .IsUnicode(false);

            modelBuilder.Entity<finance_investmentmodel>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<finance_investmentmodel>()
                .Property(e => e.status)
                .IsUnicode(false);

            modelBuilder.Entity<finance_investmentmodel>()
                .Property(e => e.version)
                .IsUnicode(false);

            modelBuilder.Entity<GoalHolding>()
                .Property(e => e.Currency)
                .IsFixedLength();

            modelBuilder.Entity<GoalHoldingsHistory>()
                .Property(e => e.Currency)
                .IsFixedLength();

            modelBuilder.Entity<Goal>()
                .Property(e => e.GoalCategory)
                .IsUnicode(false);

            modelBuilder.Entity<Goal>()
                .HasMany(e => e.GoalHoldings)
                .WithRequired(e => e.Goal)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Goal>()
                .HasMany(e => e.GoalHoldingsHistories)
                .WithRequired(e => e.Goal)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<GTDE>()
                .Property(e => e.BuySell)
                .IsFixedLength();

            modelBuilder.Entity<GTDEHistory>()
                .Property(e => e.BuySell)
                .IsFixedLength();

            modelBuilder.Entity<IntegrationFileLog>()
                .Property(e => e.FileName)
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationFileLog>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationFileLog>()
                .Property(e => e.Comments)
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationFileLog>()
                .Property(e => e.Error)
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationFileLog>()
                .HasMany(e => e.IntegrationACCTFILEs)
                .WithOptional(e => e.IntegrationFileLog)
                .HasForeignKey(e => e.FileID);

            modelBuilder.Entity<IntegrationFileLog>()
                .HasMany(e => e.IntegrationAuditLogs)
                .WithOptional(e => e.IntegrationFileLog)
                .HasForeignKey(e => e.FileID);

            modelBuilder.Entity<IntegrationFileLog>()
                .HasMany(e => e.IntegrationRecordLogs)
                .WithOptional(e => e.IntegrationFileLog)
                .HasForeignKey(e => e.FileID);

            modelBuilder.Entity<IntegrationRecordLog>()
                .Property(e => e.FileName)
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationRecordLog>()
                .Property(e => e.Action)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationRecordLog>()
                .Property(e => e.Result)
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationRecordLog>()
                .Property(e => e.Comments)
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationRecordLog>()
                .Property(e => e.Error)
                .IsUnicode(false);

            modelBuilder.Entity<LockedUser>()
                .Property(e => e.UserName)
                .IsUnicode(false);

            modelBuilder.Entity<ParentAssetClass>()
                .Property(e => e.ParrentAssetClass)
                .IsUnicode(false);

            modelBuilder.Entity<ParentAssetClass>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<ParentAssetGroup>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<PortfolioAccountFunding>()
                .Property(e => e.Currency)
                .IsFixedLength();

            modelBuilder.Entity<position_holdinggrouphistory>()
                .Property(e => e.tenant_id)
                .IsUnicode(false);

            modelBuilder.Entity<position_holdinggrouphistory>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<position_holdinggrouphistory>()
                .Property(e => e.value_basecurrencyvalue_amount)
                .HasPrecision(19, 3);

            modelBuilder.Entity<position_holdinggrouphistory>()
                .Property(e => e.value_basecurrencyvalue_currency)
                .IsUnicode(false);

            modelBuilder.Entity<position_holdinggrouphistory>()
                .Property(e => e.value_refcurrencyvalue_amount)
                .HasPrecision(19, 3);

            modelBuilder.Entity<position_holdinggrouphistory>()
                .Property(e => e.value_refcurrencyvalue_currency)
                .IsUnicode(false);

            modelBuilder.Entity<position_holdinggrouphistory>()
                .Property(e => e.investedcash_basecurrencyvalue_amount)
                .HasPrecision(19, 3);

            modelBuilder.Entity<position_holdinggrouphistory>()
                .Property(e => e.investedcash_basecurrencyvalue_currency)
                .IsUnicode(false);

            modelBuilder.Entity<position_holdinggrouphistory>()
                .Property(e => e.investedcash_refcurrencyvalue_amount)
                .HasPrecision(19, 3);

            modelBuilder.Entity<position_holdinggrouphistory>()
                .Property(e => e.investedcash_refcurrencyvalue_currency)
                .IsUnicode(false);

            modelBuilder.Entity<position_holdinggrouphistorypolicy>()
                .Property(e => e.tenant_id)
                .IsUnicode(false);

            modelBuilder.Entity<position_holdinggrouphistorypolicy>()
                .Property(e => e.recipientid)
                .IsUnicode(false);

            modelBuilder.Entity<position_holdinggrouphistorypolicy>()
                .Property(e => e.treeid)
                .IsUnicode(false);

            modelBuilder.Entity<position_holdinghistory>()
                .Property(e => e.tenant_id)
                .IsUnicode(false);

            modelBuilder.Entity<position_holdinghistory>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<position_holdinghistory>()
                .Property(e => e.type)
                .IsUnicode(false);

            modelBuilder.Entity<position_holdinghistory>()
                .Property(e => e.investedcash_basecurrencyvalue_amount)
                .HasPrecision(19, 3);

            modelBuilder.Entity<position_holdinghistory>()
                .Property(e => e.investedcash_basecurrencyvalue_currency)
                .IsUnicode(false);

            modelBuilder.Entity<position_holdinghistory>()
                .Property(e => e.investedcash_refcurrencyvalue_amount)
                .HasPrecision(19, 3);

            modelBuilder.Entity<position_holdinghistory>()
                .Property(e => e.investedcash_refcurrencyvalue_currency)
                .IsUnicode(false);

            modelBuilder.Entity<position_holdinghistory>()
                .Property(e => e.value_basecurrencyvalue_amount)
                .HasPrecision(19, 3);

            modelBuilder.Entity<position_holdinghistory>()
                .Property(e => e.value_basecurrencyvalue_currency)
                .IsUnicode(false);

            modelBuilder.Entity<position_holdinghistory>()
                .Property(e => e.value_refcurrencyvalue_amount)
                .HasPrecision(19, 3);

            modelBuilder.Entity<position_holdinghistory>()
                .Property(e => e.value_refcurrencyvalue_currency)
                .IsUnicode(false);

            modelBuilder.Entity<position_holdinghistory>()
                .Property(e => e.quantity)
                .HasPrecision(19, 3);

            modelBuilder.Entity<ProjectionData>()
                .Property(e => e.Years)
                .HasPrecision(10, 1);

            modelBuilder.Entity<ProjectionData>()
                .Property(e => e.MeanValue)
                .HasPrecision(18, 14);

            modelBuilder.Entity<ProjectionData>()
                .Property(e => e.UpValue)
                .HasPrecision(18, 14);

            modelBuilder.Entity<ProjectionData>()
                .Property(e => e.LowValue)
                .HasPrecision(18, 14);

            modelBuilder.Entity<Prospect>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<Prospect>()
                .Property(e => e.sourceip)
                .IsUnicode(false);

            modelBuilder.Entity<ra_fundingaccountinfo>()
                .Property(e => e.tenant_id)
                .IsUnicode(false);

            modelBuilder.Entity<ra_fundingaccountinfo>()
                .Property(e => e.accountname)
                .IsUnicode(false);

            modelBuilder.Entity<ra_fundingaccountinfo>()
                .Property(e => e.accountnumber)
                .IsUnicode(false);

            modelBuilder.Entity<ra_fundingaccountinfo>()
                .Property(e => e.bankcode)
                .IsUnicode(false);

            modelBuilder.Entity<ra_fundingaccountinfo>()
                .Property(e => e.bankname)
                .IsUnicode(false);

            modelBuilder.Entity<ra_fundingaccountinfo>()
                .Property(e => e.branchcode)
                .IsUnicode(false);

            modelBuilder.Entity<ra_fundingaccountinfo>()
                .Property(e => e.country)
                .IsUnicode(false);

            modelBuilder.Entity<ra_fundingaccountinfo>()
                .Property(e => e.iban)
                .IsUnicode(false);

            modelBuilder.Entity<ra_fundingaccountinfo>()
                .Property(e => e.notes)
                .IsUnicode(false);

            modelBuilder.Entity<ra_fundingaccountinfo>()
                .Property(e => e.othersource)
                .IsUnicode(false);

            modelBuilder.Entity<ra_fundingaccountinfo>()
                .Property(e => e.swiftcode)
                .IsUnicode(false);

            modelBuilder.Entity<ra_fundingaccountinfo>()
                .Property(e => e.username)
                .IsUnicode(false);

            modelBuilder.Entity<ra_fundingaccountinfo>()
                .Property(e => e.accounttypecode)
                .IsUnicode(false);

            modelBuilder.Entity<ra_fundingaccountinfo>()
                .Property(e => e.foreignbankaccountind)
                .IsUnicode(false);

            modelBuilder.Entity<ra_fundingaccountinfo>()
                .Property(e => e.futuresatementcurrency)
                .IsUnicode(false);

            modelBuilder.Entity<ra_fundingaccountinfo>()
                .Property(e => e.statementcurrency)
                .IsUnicode(false);

            modelBuilder.Entity<ra_fundingaccountinfo>()
                .Property(e => e.status)
                .IsUnicode(false);

            modelBuilder.Entity<RiskProfile>()
                .Property(e => e.Currency)
                .IsFixedLength();

            modelBuilder.Entity<ACCT>()
                .Property(e => e.AccountType)
                .IsUnicode(false);

            modelBuilder.Entity<ACCT>()
                .Property(e => e.AccountNumber)
                .IsUnicode(false);

            modelBuilder.Entity<ACCT>()
                .Property(e => e.Citizenship)
                .IsUnicode(false);

            modelBuilder.Entity<ACCT>()
                .Property(e => e.TaxResidency)
                .IsUnicode(false);

            modelBuilder.Entity<ACCT>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<ACCT>()
                .Property(e => e.FirstName1)
                .IsUnicode(false);

            modelBuilder.Entity<ACCT>()
                .Property(e => e.FirstName2)
                .IsUnicode(false);

            modelBuilder.Entity<ACCT>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<ACCT>()
                .Property(e => e.IdentityType)
                .IsUnicode(false);

            modelBuilder.Entity<ACCT>()
                .Property(e => e.PassportNo)
                .IsUnicode(false);

            modelBuilder.Entity<ACCT>()
                .Property(e => e.PassportIssueCountry)
                .IsUnicode(false);

            modelBuilder.Entity<ACCT>()
                .Property(e => e.PassportIssueDate)
                .IsUnicode(false);

            modelBuilder.Entity<ACCT>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<ACCT>()
                .Property(e => e.Country)
                .IsUnicode(false);

            modelBuilder.Entity<ACCT>()
                .Property(e => e.Address1)
                .IsUnicode(false);

            modelBuilder.Entity<ACCT>()
                .Property(e => e.Address2)
                .IsUnicode(false);

            modelBuilder.Entity<ACCT>()
                .Property(e => e.Address3)
                .IsUnicode(false);

            modelBuilder.Entity<ACCT>()
                .Property(e => e.Address4)
                .IsUnicode(false);

            modelBuilder.Entity<ACCT>()
                .Property(e => e.USCity)
                .IsUnicode(false);

            modelBuilder.Entity<ACCT>()
                .Property(e => e.NonUSCity)
                .IsUnicode(false);

            modelBuilder.Entity<ACCT>()
                .Property(e => e.USState)
                .IsUnicode(false);

            modelBuilder.Entity<ACCT>()
                .Property(e => e.USPostalCode)
                .IsUnicode(false);

            modelBuilder.Entity<ACCTHistory>()
                .Property(e => e.AccountType)
                .IsUnicode(false);

            modelBuilder.Entity<ACCTHistory>()
                .Property(e => e.AccountNumber)
                .IsUnicode(false);

            modelBuilder.Entity<ACCTHistory>()
                .Property(e => e.Citizenship)
                .IsUnicode(false);

            modelBuilder.Entity<ACCTHistory>()
                .Property(e => e.TaxResidency)
                .IsUnicode(false);

            modelBuilder.Entity<ACCTHistory>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<ACCTHistory>()
                .Property(e => e.FirstName1)
                .IsUnicode(false);

            modelBuilder.Entity<ACCTHistory>()
                .Property(e => e.FirstName2)
                .IsUnicode(false);

            modelBuilder.Entity<ACCTHistory>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<ACCTHistory>()
                .Property(e => e.IdentityType)
                .IsUnicode(false);

            modelBuilder.Entity<ACCTHistory>()
                .Property(e => e.PassportNo)
                .IsUnicode(false);

            modelBuilder.Entity<ACCTHistory>()
                .Property(e => e.PassportIssueCountry)
                .IsUnicode(false);

            modelBuilder.Entity<ACCTHistory>()
                .Property(e => e.PassportIssueDate)
                .IsUnicode(false);

            modelBuilder.Entity<ACCTHistory>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<ACCTHistory>()
                .Property(e => e.Country)
                .IsUnicode(false);

            modelBuilder.Entity<ACCTHistory>()
                .Property(e => e.Address1)
                .IsUnicode(false);

            modelBuilder.Entity<ACCTHistory>()
                .Property(e => e.Address2)
                .IsUnicode(false);

            modelBuilder.Entity<ACCTHistory>()
                .Property(e => e.Address3)
                .IsUnicode(false);

            modelBuilder.Entity<ACCTHistory>()
                .Property(e => e.Address4)
                .IsUnicode(false);

            modelBuilder.Entity<ACCTHistory>()
                .Property(e => e.USCity)
                .IsUnicode(false);

            modelBuilder.Entity<ACCTHistory>()
                .Property(e => e.NonUSCity)
                .IsUnicode(false);

            modelBuilder.Entity<ACCTHistory>()
                .Property(e => e.USState)
                .IsUnicode(false);

            modelBuilder.Entity<ACCTHistory>()
                .Property(e => e.USPostalCode)
                .IsUnicode(false);

            modelBuilder.Entity<CustomersHistory>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<CustomersHistory>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<CustomersHistory>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<CustomersHistory>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<CustomersHistory>()
                .Property(e => e.Mobile)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<CustomersHistory>()
                .Property(e => e.OfficePhone)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<CustomersHistory>()
                .Property(e => e.Address)
                .IsUnicode(false);

            modelBuilder.Entity<CustomersHistory>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<CustomersHistory>()
                .Property(e => e.State)
                .IsUnicode(false);

            modelBuilder.Entity<CustomersHistory>()
                .Property(e => e.Country)
                .IsUnicode(false);

            modelBuilder.Entity<CustomersHistory>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<CustomersHistory>()
                .Property(e => e.Citizenship)
                .IsUnicode(false);

            modelBuilder.Entity<CustomersHistory>()
                .Property(e => e.TaxResidency)
                .IsUnicode(false);

            modelBuilder.Entity<CustomersHistory>()
                .Property(e => e.PassportNo)
                .IsUnicode(false);

            modelBuilder.Entity<CustomersHistory>()
                .Property(e => e.PassportIssueCountry)
                .IsUnicode(false);

            modelBuilder.Entity<CustomersHistory>()
                .Property(e => e.NRIC)
                .IsUnicode(false);

            modelBuilder.Entity<CustomersHistory>()
                .Property(e => e.PostalCode)
                .IsUnicode(false);

            modelBuilder.Entity<GCU>()
                .Property(e => e.CSIndicator)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationACCTFILE>()
                .Property(e => e.FileName)
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationACCTFILE>()
                .Property(e => e.RecordCount)
                .IsFixedLength();

            modelBuilder.Entity<IntegrationACCTFILE>()
                .Property(e => e.TransactionType)
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationACCTFILE>()
                .Property(e => e.RecordType)
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationACCTFILE>()
                .Property(e => e.AType)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationACCTFILE>()
                .Property(e => e.BType)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationACCTFILE>()
                .Property(e => e.CType)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationACCTFILE>()
                .Property(e => e.WType)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationACCTFILE>()
                .Property(e => e.FourType)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationACCTFILE>()
                .Property(e => e.GType)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationACCTFILE>()
                .Property(e => e.HType)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationACCTFILE>()
                .Property(e => e.XType)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationACCTFILE>()
                .Property(e => e.MType)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationACCTFILE>()
                .Property(e => e.IType)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationACCTFILE>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationACCTFILE>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationACCTFILE>()
                .Property(e => e.AccountNumber)
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationACCTFILE>()
                .Property(e => e.Nationality)
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationACCTFILE>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationACCTFILE>()
                .Property(e => e.Address)
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationACCTFILE>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationACCTFILE>()
                .Property(e => e.State)
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationACCTFILE>()
                .Property(e => e.CountryCode)
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationACCTFILE>()
                .Property(e => e.PostalCode)
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationACCTFILE>()
                .Property(e => e.PassportNumber)
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationACCTFILE>()
                .Property(e => e.PassportIssuingCountry)
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationACCTFILE>()
                .Property(e => e.PassportExpiredDate)
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationACCTFILE>()
                .Property(e => e.PassportIssuingDate)
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationACCTFILE>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationACCTFILE>()
                .Property(e => e.TaxResidence)
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationAuditLog>()
                .Property(e => e.FileName)
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationAuditLog>()
                .Property(e => e.TableName)
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationAuditLog>()
                .Property(e => e.ColumnName)
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationAuditLog>()
                .Property(e => e.PKColumnValue)
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationAuditLog>()
                .Property(e => e.OldValue)
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationAuditLog>()
                .Property(e => e.NewValue)
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationAuditLog>()
                .Property(e => e.Action)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationAuditLog>()
                .Property(e => e.Comments)
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationAuditLog>()
                .Property(e => e.LoggedBy)
                .IsUnicode(false);

            modelBuilder.Entity<IntegrationAuditLog>()
                .Property(e => e.ModifiedBy)
                .IsUnicode(false);
        }
    }
}
