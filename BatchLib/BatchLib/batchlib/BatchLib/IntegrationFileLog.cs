namespace BatchLib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("IntegrationFileLog")]
    public partial class IntegrationFileLog
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public IntegrationFileLog()
        {
            IntegrationACCTFILEs = new HashSet<IntegrationACCTFILE>();
            IntegrationAuditLogs = new HashSet<IntegrationAuditLog>();
            IntegrationRecordLogs = new HashSet<IntegrationRecordLog>();
        }

        public int ID { get; set; }

        [StringLength(50)]
        public string FileName { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        [StringLength(20)]
        public string Status { get; set; }

        [StringLength(500)]
        public string Comments { get; set; }

        [StringLength(500)]
        public string Error { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IntegrationACCTFILE> IntegrationACCTFILEs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IntegrationAuditLog> IntegrationAuditLogs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IntegrationRecordLog> IntegrationRecordLogs { get; set; }
    }
}
