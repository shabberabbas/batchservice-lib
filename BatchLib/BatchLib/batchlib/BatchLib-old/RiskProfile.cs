using System;

namespace BatchLib
{
	public class RiskProfile
	{
		public long id
		{
			get;
			set;
		}

		public string RiskLongDesc
		{
			get;
			set;
		}

		public string RiskDesc
		{
			get;
			set;
		}

		public bool disable
		{
			get;
			set;
		}

		public DateTime? Created
		{
			get;
			set;
		}

		public DateTime? Modified
		{
			get;
			set;
		}

		public string CreatedBy
		{
			get;
			set;
		}

		public string ModifiedBy
		{
			get;
			set;
		}

		public int? Risk
		{
			get;
			set;
		}

		public long? CatalogueItemId
		{
			get;
			set;
		}
	}
}
