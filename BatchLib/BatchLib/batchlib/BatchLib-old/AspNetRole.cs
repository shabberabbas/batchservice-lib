using System.Collections.Generic;

namespace BatchLib
{
	public class AspNetRole
	{
		public string Id
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public virtual ICollection<AspNetUser> AspNetUsers
		{
			get;
			set;
		}

		public AspNetRole()
		{
			this.AspNetUsers = new HashSet<AspNetUser>();
		}
	}
}
