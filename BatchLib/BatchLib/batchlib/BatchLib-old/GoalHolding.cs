using System;

namespace BatchLib
{
	public class GoalHolding
	{
		public long id
		{
			get;
			set;
		}

		public long GoalId
		{
			get;
			set;
		}

		public long? CurrentHoldingUnits
		{
			get;
			set;
		}

		public long? InitiaHoldingUnits
		{
			get;
			set;
		}

		public DateTime? HoldingDate
		{
			get;
			set;
		}

		public decimal? InitialInvestment
		{
			get;
			set;
		}

		public decimal? InvestedCash
		{
			get;
			set;
		}

		public decimal? InvestmentValue
		{
			get;
			set;
		}

		public decimal? PurchasePrice
		{
			get;
			set;
		}

		public string Currency
		{
			get;
			set;
		}

		public bool disable
		{
			get;
			set;
		}

		public DateTime? Modified
		{
			get;
			set;
		}

		public string ModifiedBy
		{
			get;
			set;
		}

		public DateTime? Created
		{
			get;
			set;
		}

		public string CreatedBy
		{
			get;
			set;
		}

		public virtual Goal Goal
		{
			get;
			set;
		}
	}
}
