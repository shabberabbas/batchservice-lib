using System;

namespace BatchLib
{
	public class CustomerStatu
	{
		public int id
		{
			get;
			set;
		}

		public string Status
		{
			get;
			set;
		}

		public string StatusText
		{
			get;
			set;
		}

		public DateTime? Created
		{
			get;
			set;
		}

		public string CreatedBy
		{
			get;
			set;
		}

		public DateTime? Modified
		{
			get;
			set;
		}

		public string ModifiedBy
		{
			get;
			set;
		}

		public bool disable
		{
			get;
			set;
		}
	}
}
