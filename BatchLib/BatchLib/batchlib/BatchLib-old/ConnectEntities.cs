using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace BatchLib
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ConnectEntities : DbContext 
    {
        public ConnectEntities()
            : base("name=ConnectEntities")
        {
        }

        public virtual DbSet<LockedUser> LockedUsers { get; set; }
        public virtual DbSet<CatalogueItem> CatalogueItems { get; set; }
        public virtual DbSet<CatItemHistoryPrice> CatItemHistoryPrices { get; set; }
        public virtual DbSet<GoalHoldingsHistory> GoalHoldingsHistories { get; set; }
        public virtual DbSet<CustomerDoc> CustomerDocs { get; set; }
        public virtual DbSet<CustomerPortfolioAccount> CustomerPortfolioAccounts { get; set; }
        public virtual DbSet<CatalogueItemHistory> CatalogueItemHistories { get; set; }
        public virtual DbSet<CustomerPortfolioAccountHistory> CustomerPortfolioAccountHistories { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<CustomersHistory> CustomersHistories { get; set; }
        //public virtual DbSet<CustomerHistory> CustomersHistory { get; set; }
        public virtual DbSet<GoalHolding> GoalHoldings { get; set; }
        public virtual DbSet<Goal> Goals { get; set; }
        public virtual DbSet<Prospect> Prospects { get; set; }
        public virtual DbSet<RiskProfile> RiskProfiles { get; set; }
        public virtual DbSet<TransactionLog> TransactionLogs { get; set; }

        //Graph

        public virtual DbSet<ProjectionData> ProjectionDatas { get; set; }
        public virtual DbSet<ParentAssetClass> ParentAssetClasses { get; set; }
        public virtual DbSet<ParentAssetGroup> ParentAssetGroups { get; set; }
        public virtual DbSet<Breakdown> Breakdowns { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CatalogueItem>()
                .Property(e => e.Ticker)
                .IsFixedLength();

            modelBuilder.Entity<CatalogueItem>()
                .Property(e => e.Registration)
                .IsFixedLength();

            modelBuilder.Entity<CatalogueItem>()
                .Property(e => e.Listing)
                .IsFixedLength();

            modelBuilder.Entity<CatalogueItem>()
                .Property(e => e.Currency)
                .IsFixedLength();

            modelBuilder.Entity<CatalogueItem>()
                .HasMany(e => e.CatItemHistoryPrices)
                .WithRequired(e => e.CatalogueItem)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CatItemHistoryPrice>()
                .Property(e => e.Currency)
                .IsFixedLength();

            modelBuilder.Entity<CustomerPortfolioAccount>()
                .Property(e => e.Currency)
                .IsFixedLength();

            modelBuilder.Entity<Customer>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.Mobile)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.OfficePhone)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.Address)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.State)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.Country)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.Citizenship)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.TaxResidency)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.PassportNo)
                .IsUnicode(false);

            /*
            modelBuilder.Entity<Customer>()
                .Property(e => e.PassportIssueCountry)
                .IsUnicode(false);
            */

            modelBuilder.Entity<GoalHolding>()
                .Property(e => e.Currency)
                .IsFixedLength();

            modelBuilder.Entity<Goal>()
                .Property(e => e.GoalCategory)
                .IsUnicode(false);

            /*
            modelBuilder.Entity<Goal>()
                .HasMany(e => e.GoalHoldings)
                .WithRequired(e => e.Goal)
                .WillCascadeOnDelete(false);
             */

            modelBuilder.Entity<Prospect>()
                .Property(e => e.email)
                .IsUnicode(false);

            /*
            modelBuilder.Entity<Prospect>()
                .Property(e => e.sourceip)
                .IsUnicode(false);
            */
        }
        
	}
}
