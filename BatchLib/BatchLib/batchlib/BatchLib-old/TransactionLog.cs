using System;

namespace BatchLib
{
	public class TransactionLog
	{
		public long id
		{
			get;
			set;
		}

		public decimal? Amount
		{
			get;
			set;
		}

		public long? TotalUnits
		{
			get;
			set;
		}

		public long? CustomerId
		{
			get;
			set;
		}

		public int? txType
		{
			get;
			set;
		}

		public string txStatus
		{
			get;
			set;
		}

		public string transactionRefNo
		{
			get;
			set;
		}

		public long? GoalId
		{
			get;
			set;
		}

		public string CreatedBy
		{
			get;
			set;
		}

		public string ModifiedBy
		{
			get;
			set;
		}

		public DateTime? Created
		{
			get;
			set;
		}

		public DateTime? Modified
		{
			get;
			set;
		}

		public string Currency
		{
			get;
			set;
		}

		public bool disable
		{
			get;
			set;
		}

		public virtual Customer Customer
		{
			get;
			set;
		}

		public virtual Goal Goal
		{
			get;
			set;
		}
	}
}
