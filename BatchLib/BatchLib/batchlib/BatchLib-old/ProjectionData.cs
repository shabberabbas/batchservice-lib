namespace BatchLib
{
	public class ProjectionData
	{
		public int Id
		{
			get;
			set;
		}

		public int? CatalogueItemId
		{
			get;
			set;
		}

		public int? NoDays
		{
			get;
			set;
		}

		public decimal? Years
		{
			get;
			set;
		}

		public decimal? MeanPercentage
		{
			get;
			set;
		}

		public decimal? UpPercentage
		{
			get;
			set;
		}

		public decimal? LowPercentage
		{
			get;
			set;
		}

		public decimal? MeanValue
		{
			get;
			set;
		}

		public decimal? UpValue
		{
			get;
			set;
		}

		public decimal? LowValue
		{
			get;
			set;
		}

		public virtual CatalogueItem CatalogueItem
		{
			get;
			set;
		}
	}
}
