using System;

namespace BatchLib
{
	public class Customer1
	{
		public int id
		{
			get;
			set;
		}

		public string userActivationURL
		{
			get;
			set;
		}

		public string email
		{
			get;
			set;
		}

		public string FirstName
		{
			get;
			set;
		}

		public string LastName
		{
			get;
			set;
		}

		public string Phone
		{
			get;
			set;
		}

		public string Mobile
		{
			get;
			set;
		}

		public string OfficePhone
		{
			get;
			set;
		}

		public string Address
		{
			get;
			set;
		}

		public string City
		{
			get;
			set;
		}

		public string State
		{
			get;
			set;
		}

		public string Country
		{
			get;
			set;
		}

		public string Status
		{
			get;
			set;
		}

		public string Password
		{
			get;
			set;
		}

		public Guid ActivationCode
		{
			get;
			set;
		}

		public DateTime? CreatedDate
		{
			get;
			set;
		}

		public int? ModifiedBy
		{
			get;
			set;
		}

		public DateTime? ModifiedDate
		{
			get;
			set;
		}

		public bool disable
		{
			get;
			set;
		}
	}
}
