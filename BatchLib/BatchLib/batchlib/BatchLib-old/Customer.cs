using System;
using System.Collections.Generic;

namespace BatchLib
{
	public class Customer
	{
		public long id
		{
			get;
			set;
		}

		public string email
		{
			get;
			set;
		}

		public string FirstName
		{
			get;
			set;
		}

		public string LastName
		{
			get;
			set;
		}

		public string Phone
		{
			get;
			set;
		}

		public string Mobile
		{
			get;
			set;
		}

		public string OfficePhone
		{
			get;
			set;
		}

		public string Address
		{
			get;
			set;
		}

		public string City
		{
			get;
			set;
		}

		public string State
		{
			get;
			set;
		}

		public string Country
		{
			get;
			set;
		}

		public string Status
		{
			get;
			set;
		}

		public DateTime? Created
		{
			get;
			set;
		}

		public string ModifiedBy
		{
			get;
			set;
		}

		public DateTime? Modified
		{
			get;
			set;
		}

		public bool disable
		{
			get;
			set;
		}

		public Guid? ActivationCode
		{
			get;
			set;
		}

		public string Password
		{
			get;
			set;
		}

		public string Alias
		{
			get;
			set;
		}

		public string MothersMaidenName
		{
			get;
			set;
		}

		public DateTime? birthDate
		{
			get;
			set;
		}

		public string Citizenship
		{
			get;
			set;
		}

		public string TaxResidency
		{
			get;
			set;
		}

		public string SourceOfFundingOptions
		{
			get;
			set;
		}

		public string BankDetails
		{
			get;
			set;
		}

		public string LocalBankDetails
		{
			get;
			set;
		}

		public string PassportNo
		{
			get;
			set;
		}

		public DateTime? PassportIssueDate
		{
			get;
			set;
		}

		public DateTime? PassportExpiryDate
		{
			get;
			set;
		}

		public string PassportIssueCountry
		{
			get;
			set;
		}

		public bool? LockOutEmailSentFlag
		{
			get;
			set;
		}

		public string CreatedBy
		{
			get;
			set;
		}

		public string NRIC
		{
			get;
			set;
		}

		public string PostalCode
		{
			get;
			set;
		}

		public virtual ICollection<CustomerDoc> CustomerDocs
		{
			get;
			set;
		}

		public virtual ICollection<CustomerPortfolioAccount> CustomerPortfolioAccounts
		{
			get;
			set;
		}

		public virtual ICollection<CustomerPortfolioAccountHistory> CustomerPortfolioAccountHistories
		{
			get;
			set;
		}

		public virtual ICollection<Goal> Goals
		{
			get;
			set;
		}

		public virtual ICollection<PortfolioAccountFunding> PortfolioAccountFundings
		{
			get;
			set;
		}

		public virtual ICollection<TransactionLog> TransactionLogs
		{
			get;
			set;
		}

		public Customer()
		{
			this.CustomerDocs = new HashSet<CustomerDoc>();
			this.CustomerPortfolioAccounts = new HashSet<CustomerPortfolioAccount>();
			this.CustomerPortfolioAccountHistories = new HashSet<CustomerPortfolioAccountHistory>();
			this.Goals = new HashSet<Goal>();
			this.PortfolioAccountFundings = new HashSet<PortfolioAccountFunding>();
			this.TransactionLogs = new HashSet<TransactionLog>();
		}
	}
}
