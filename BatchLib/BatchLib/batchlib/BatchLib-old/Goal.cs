using System;
using System.Collections.Generic;

namespace BatchLib
{
	public class Goal
	{
		public long id
		{
			get;
			set;
		}

		public long? CustomerId
		{
			get;
			set;
		}

		public string GoalCategory
		{
			get;
			set;
		}

		public int? Risk
		{
			get;
			set;
		}

		public int? RiskProfileId
		{
			get;
			set;
		}

		public int? ProductId
		{
			get;
			set;
		}

		public decimal TargetAmount
		{
			get;
			set;
		}

		public decimal InitialInvestment
		{
			get;
			set;
		}

		public int Term
		{
			get;
			set;
		}

		public decimal? ProjectedOptimisticScenario
		{
			get;
			set;
		}

		public decimal? ProjectedMediumScenario
		{
			get;
			set;
		}

		public decimal? ProjectedPessimisticScenario
		{
			get;
			set;
		}

		public string CreatedBy
		{
			get;
			set;
		}

		public DateTime? Created
		{
			get;
			set;
		}

		public string ModifiedBy
		{
			get;
			set;
		}

		public DateTime? Modified
		{
			get;
			set;
		}

		public bool disable
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public bool isRiskVerified
		{
			get;
			set;
		}

		public virtual Customer Customer
		{
			get;
			set;
		}

		public virtual ICollection<GoalHolding> GoalHoldings
		{
			get;
			set;
		}

		public virtual ICollection<GoalHoldingsHistory> GoalHoldingsHistories
		{
			get;
			set;
		}

		public virtual ICollection<TransactionLog> TransactionLogs
		{
			get;
			set;
		}

		public Goal()
		{
			this.GoalHoldings = new HashSet<GoalHolding>();
			this.GoalHoldingsHistories = new HashSet<GoalHoldingsHistory>();
			this.TransactionLogs = new HashSet<TransactionLog>();
		}
	}
}
