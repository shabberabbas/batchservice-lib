using System.Data.Entity;

namespace BatchLib
{
	public class riabadminconsoleEntities:DbContext
	{
        public riabadminconsoleEntities()
            : base("name=adminconsole")
        {
        }
        public virtual DbSet AuditLogs
		{
			get;
			set;
		}
        
		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
		}
	}
}