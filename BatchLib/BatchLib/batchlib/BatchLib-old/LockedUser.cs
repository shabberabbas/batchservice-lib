using System;

namespace BatchLib
{
	public class LockedUser
	{
		public long id
		{
			get;
			set;
		}

		public string UserName
		{
			get;
			set;
		}

		public DateTime? LockedDate
		{
			get;
			set;
		}

		public bool disable
		{
			get;
			set;
		}
	}
}
