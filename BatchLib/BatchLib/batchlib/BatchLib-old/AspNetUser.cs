using System;
using System.Collections.Generic;

namespace BatchLib
{
	public class AspNetUser
	{
		public string Id
		{
			get;
			set;
		}

		public string FirstName
		{
			get;
			set;
		}

		public string LastName
		{
			get;
			set;
		}

		public byte Level
		{
			get;
			set;
		}

		public DateTime JoinDate
		{
			get;
			set;
		}

		public string Email
		{
			get;
			set;
		}

		public bool EmailConfirmed
		{
			get;
			set;
		}

		public string PasswordHash
		{
			get;
			set;
		}

		public string SecurityStamp
		{
			get;
			set;
		}

		public string PhoneNumber
		{
			get;
			set;
		}

		public bool PhoneNumberConfirmed
		{
			get;
			set;
		}

		public bool TwoFactorEnabled
		{
			get;
			set;
		}

		public DateTime? LockoutEndDateUtc
		{
			get;
			set;
		}

		public bool LockoutEnabled
		{
			get;
			set;
		}

		public int AccessFailedCount
		{
			get;
			set;
		}

		public string UserName
		{
			get;
			set;
		}

		public virtual ICollection<AspNetUserClaim> AspNetUserClaims
		{
			get;
			set;
		}

		public virtual ICollection<AspNetUserLogin> AspNetUserLogins
		{
			get;
			set;
		}

		public virtual ICollection<AspNetRole> AspNetRoles
		{
			get;
			set;
		}

		public AspNetUser()
		{
			this.AspNetUserClaims = new HashSet<AspNetUserClaim>();
			this.AspNetUserLogins = new HashSet<AspNetUserLogin>();
			this.AspNetRoles = new HashSet<AspNetRole>();
		}
	}
}
