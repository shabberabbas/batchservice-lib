using log4net;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.Reflection;

namespace BatchLib
{
	public class CatalogueBatchService
	{
		private static ConnectEntities db = new ConnectEntities();

		private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private static string ItemCatHistoryFilesPath = ConfigurationSettings.AppSettings["ItemCatHistoryFilesPath"];

		public static bool GenerateCatalogueHistoryPriceFiles()
		{
			//IL_0006: Unknown result type (might be due to invalid IL or missing references)
			//IL_00b2: Expected O, but got Unknown
			//IL_00f0: Unknown result type (might be due to invalid IL or missing references)
			//IL_01b8: Expected O, but got Unknown
			//IL_075b: Unknown result type (might be due to invalid IL or missing references)
			//IL_077f: Unknown result type (might be due to invalid IL or missing references)
			var source = (from c in (IQueryable<CatalogueItem>)CatalogueBatchService.db.CatalogueItems
			select new
			{
				c.id,
				c.Name
			}).ToList();
			for (int i = 0; i < source.Count(); i++)
			{
				string name = source.ElementAt(i).Name;
				int id = source.ElementAt(i).id;
				var array = (from x in (IQueryable<CatalogueItemHistory>)CatalogueBatchService.db.CatalogueItemHistories
				where x.Name == name && x.LatestPrice.HasValue && x.Modified.HasValue
				group x by new
				{
					x.Name,
					x.Modified,
					x.LatestPriceDate,
					x.LatestPrice,
					x.Currency
				} into g
				let Modified = g.Max((CatalogueItemHistory y) => y.Modified)
				select new
				{
					name = g.Key.Name,
					modifieddate = g.Key.Modified.Value,
					latestpricedate = g.Key.LatestPriceDate,
					latestprice_refcurrencyvalue_amount = g.Key.LatestPrice,
					latestprice_basecurrencyvalue_currency = g.Key.Currency.Trim()
				}).ToArray();
				string contents = JsonConvert.SerializeObject((object)array);
				try
				{
					File.WriteAllText(CatalogueBatchService.ItemCatHistoryFilesPath + name.Replace(" ", "-") + ".json", contents);
				}
				catch (Exception ex)
				{
					if (ex != null)
					{
						CatalogueBatchService.log.Error((object)ex.Message);
					}
					if (ex.InnerException != null)
					{
						CatalogueBatchService.log.Error((object)ex.InnerException.Message);
					}
				}
			}
			EmailService.SendInternalEmail("CONNECT PROCESS ItemCatalogueHistoryPriceFiles", "Item Catalogue History Price files has been generated at " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"));
			return true;
		}
	}
}
