using System;

namespace BatchLib
{
	public class CustomerDoc
	{
		public long id
		{
			get;
			set;
		}

		public long? CustomerId
		{
			get;
			set;
		}

		public string Doctype
		{
			get;
			set;
		}

		public string Filename
		{
			get;
			set;
		}

		public byte[] File
		{
			get;
			set;
		}

		public bool isVerified
		{
			get;
			set;
		}

		public DateTime Created
		{
			get;
			set;
		}

		public DateTime? Modified
		{
			get;
			set;
		}

		public string CreatedBy
		{
			get;
			set;
		}

		public string ModifiedBy
		{
			get;
			set;
		}

		public bool disable
		{
			get;
			set;
		}

		public virtual Customer Customer
		{
			get;
			set;
		}
	}
}
