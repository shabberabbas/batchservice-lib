using System;

namespace BatchLib
{
	public class CatItemHistoryPrice
	{
		public int id
		{
			get;
			set;
		}

		public int CatalogueItemId
		{
			get;
			set;
		}

		public decimal? LatestPrice
		{
			get;
			set;
		}

		public DateTime? LatestPriceDate
		{
			get;
			set;
		}

		public string Currency
		{
			get;
			set;
		}

		public bool? disable
		{
			get;
			set;
		}

		public DateTime? Modified
		{
			get;
			set;
		}

		public string ModifiedBy
		{
			get;
			set;
		}

		public DateTime? Created
		{
			get;
			set;
		}

		public virtual CatalogueItem CatalogueItem
		{
			get;
			set;
		}
	}
}
