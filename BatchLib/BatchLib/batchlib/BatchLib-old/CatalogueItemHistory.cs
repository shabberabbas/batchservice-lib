using System;

namespace BatchLib
{
	public class CatalogueItemHistory
	{
		public int id
		{
			get;
			set;
		}

		public int CatalogueItemId
		{
			get;
			set;
		}

		public int? ProductTypeId
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public string BusinessCode
		{
			get;
			set;
		}

		public string ISIN
		{
			get;
			set;
		}

		public string Ticker
		{
			get;
			set;
		}

		public string Registration
		{
			get;
			set;
		}

		public string Listing
		{
			get;
			set;
		}

		public int? LotSize
		{
			get;
			set;
		}

		public string Currency
		{
			get;
			set;
		}

		public decimal? Settlement
		{
			get;
			set;
		}

		public decimal? AskPrice
		{
			get;
			set;
		}

		public decimal? BidPrice
		{
			get;
			set;
		}

		public decimal? FaceValue
		{
			get;
			set;
		}

		public decimal? Price
		{
			get;
			set;
		}

		public decimal? Score
		{
			get;
			set;
		}

		public decimal? Value
		{
			get;
			set;
		}

		public DateTime? Modified
		{
			get;
			set;
		}

		public string ModifiedBy
		{
			get;
			set;
		}

		public DateTime? PreviousPriceDate
		{
			get;
			set;
		}

		public decimal? PreviousPrice
		{
			get;
			set;
		}

		public decimal? LatestPrice
		{
			get;
			set;
		}

		public DateTime? LatestPriceDate
		{
			get;
			set;
		}

		public DateTime? Created
		{
			get;
			set;
		}

		public string CreatedBy
		{
			get;
			set;
		}

		public bool? disable
		{
			get;
			set;
		}
	}
}
