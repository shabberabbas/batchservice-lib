using System.Collections.Generic;

namespace BatchLib
{
	public class ParentAssetGroup
	{
		public int Id
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public virtual ICollection<ParentAssetClass> ParentAssetClasses
		{
			get;
			set;
		}

		public ParentAssetGroup()
		{
			this.ParentAssetClasses = new HashSet<ParentAssetClass>();
		}
	}
}
