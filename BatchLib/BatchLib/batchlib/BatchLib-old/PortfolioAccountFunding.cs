using System;

namespace BatchLib
{
	public class PortfolioAccountFunding
	{
		public long id
		{
			get;
			set;
		}

		public long? CustomerId
		{
			get;
			set;
		}

		public long? PortfolioAccountId
		{
			get;
			set;
		}

		public decimal? Amount
		{
			get;
			set;
		}

		public string Currency
		{
			get;
			set;
		}

		public DateTime? Created
		{
			get;
			set;
		}

		public DateTime? Modified
		{
			get;
			set;
		}

		public string CreatedBy
		{
			get;
			set;
		}

		public string ModifiedBy
		{
			get;
			set;
		}

		public bool disable
		{
			get;
			set;
		}

		public virtual CustomerPortfolioAccount CustomerPortfolioAccount
		{
			get;
			set;
		}

		public virtual Customer Customer
		{
			get;
			set;
		}
	}
}
