using System;
using System.Collections.Generic;

namespace BatchLib
{
	public class CustomerPortfolioAccount
	{
		public long id
		{
			get;
			set;
		}

		public long? CustomerId
		{
			get;
			set;
		}

		public decimal? balanceCash
		{
			get;
			set;
		}

		public decimal? InitialInvestment
		{
			get;
			set;
		}

		public decimal? LatestInvestment
		{
			get;
			set;
		}

		public decimal? investedCash
		{
			get;
			set;
		}

		public decimal? investmentValue
		{
			get;
			set;
		}

		public string Currency
		{
			get;
			set;
		}

		public string AccountNumber
		{
			get;
			set;
		}

		public DateTime? Created
		{
			get;
			set;
		}

		public DateTime? Modified
		{
			get;
			set;
		}

		public string CreatedBy
		{
			get;
			set;
		}

		public string ModifiedBy
		{
			get;
			set;
		}

		public bool disable
		{
			get;
			set;
		}

		public virtual Customer Customer
		{
			get;
			set;
		}

		public virtual ICollection<PortfolioAccountFunding> PortfolioAccountFundings
		{
			get;
			set;
		}

		public CustomerPortfolioAccount()
		{
			this.PortfolioAccountFundings = new HashSet<PortfolioAccountFunding>();
		}
	}
}
