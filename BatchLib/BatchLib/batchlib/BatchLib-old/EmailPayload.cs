namespace BatchLib
{
	public class EmailPayload
	{
		public string FromEmail
		{
			get;
			set;
		}

		public string ToEmail
		{
			get;
			set;
		}

		public string EmailBody
		{
			get;
			set;
		}

		public string EmailSubject
		{
			get;
			set;
		}

		public string FromeName
		{
			get;
			set;
		}

		public string CcEmail
		{
			get;
			set;
		}

		public string EmailType
		{
			get;
			set;
		}

		public Customer1 customer
		{
			get;
			set;
		}
	}
}
