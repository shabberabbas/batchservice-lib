namespace BatchLib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CustomersHistory")]
    public partial class CustomersHistory
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long id { get; set; }

        [StringLength(100)]
        public string email { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(15)]
        public string Phone { get; set; }

        [StringLength(15)]
        public string Mobile { get; set; }

        [StringLength(15)]
        public string OfficePhone { get; set; }

        [StringLength(250)]
        public string Address { get; set; }

        [StringLength(20)]
        public string City { get; set; }

        [StringLength(20)]
        public string State { get; set; }

        [StringLength(25)]
        public string Country { get; set; }

        [StringLength(60)]
        public string Status { get; set; }

        public DateTime? Created { get; set; }

        [StringLength(100)]
        public string ModifiedBy { get; set; }

        public DateTime? Modified { get; set; }

        [Key]
        [Column(Order = 1)]
        public bool disable { get; set; }

        public Guid? ActivationCode { get; set; }

        public string Password { get; set; }

        [StringLength(50)]
        public string Alias { get; set; }

        [StringLength(100)]
        public string MothersMaidenName { get; set; }

        [Column(TypeName = "date")]
        public DateTime? birthDate { get; set; }

        [StringLength(100)]
        public string Citizenship { get; set; }

        [StringLength(100)]
        public string TaxResidency { get; set; }

        public string SourceOfFundingOptions { get; set; }

        public string BankDetails { get; set; }

        public string LocalBankDetails { get; set; }

        [StringLength(20)]
        public string PassportNo { get; set; }

        [Column(TypeName = "date")]
        public DateTime? PassportIssueDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? PassportExpiryDate { get; set; }

        [StringLength(100)]
        public string PassportIssueCountry { get; set; }

        public bool? LockOutEmailSentFlag { get; set; }

        [StringLength(100)]
        public string CreatedBy { get; set; }

        [StringLength(30)]
        public string NRIC { get; set; }

        [StringLength(20)]
        public string PostalCode { get; set; }
    }
}
