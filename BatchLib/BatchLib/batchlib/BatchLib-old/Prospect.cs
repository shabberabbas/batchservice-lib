using System;

namespace BatchLib
{
	public class Prospect
	{
		public string email
		{
			get;
			set;
		}

		public DateTime? Created
		{
			get;
			set;
		}

		public string sourceip
		{
			get;
			set;
		}

		public int id
		{
			get;
			set;
		}
	}
}
