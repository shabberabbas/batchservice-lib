using BatchLib.ViewModels.ConsoleModel;
using System;
using System.Threading.Tasks;

namespace BatchLib
{
	public static class RecordAuditEntry
	{
		public static async Task<bool> RecordEntry(long CustomerId, long GoalId, string ActionDesc, string Source, string CreatedBy)
		{
			riabadminconsoleEntities db = new riabadminconsoleEntities();
			AuditLog auditLog = new AuditLog
			{
				ActionDesc = ActionDesc,
				CustomerId = CustomerId,
				GoalId = GoalId,
				Source = Source,
				Created = DateTime.Now,
				CreatedBy = CreatedBy
			};
			try
			{
				db.AuditLogs.Add(auditLog);
				if (await db.SaveChangesAsync() > 0)
				{
					return true;
				}
			}
			catch (Exception)
			{
			}
			return await Task.FromResult(true);
		}
	}
}
