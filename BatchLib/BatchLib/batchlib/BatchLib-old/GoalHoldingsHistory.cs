using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace BatchLib
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("GoalHoldingsHistory")]
    public partial class GoalHoldingsHistory
    {
        public long? id { get; set; }

        public long GoalId { get; set; }

        public long? CurrentHoldingUnits { get; set; }

        public long? InitiaHoldingUnits { get; set; }

        public DateTime? HoldingDate { get; set; }

        public decimal? InitialInvestment { get; set; }

        public decimal? InvestedCash { get; set; }

        public decimal? InvestmentValue { get; set; }

        public decimal? PurchasePrice { get; set; }

        [StringLength(6)]
        public string Currency { get; set; }

        public bool? disable { get; set; }

        public DateTime? Modified { get; set; }

        [StringLength(128)]
        public string ModifiedBy { get; set; }

        public DateTime? Created { get; set; }

        [StringLength(100)]
        public string CreatedBy { get; set; }

        [Key]
        public long OriginalId { get; set; }
    }
}
