using System.Collections.Generic;

namespace BatchLib
{
	public class ParentAssetClass
	{
		public int Id
		{
			get;
			set;
		}

		public int? ParentAssetGroupId
		{
			get;
			set;
		}

		public string ParrentAssetClass
		{
			get;
			set;
		}

		public string Description
		{
			get;
			set;
		}

		public virtual ICollection<Breakdown> Breakdowns
		{
			get;
			set;
		}

		public virtual ParentAssetGroup ParentAssetGroup
		{
			get;
			set;
		}

		public ParentAssetClass()
		{
			this.Breakdowns = new HashSet<Breakdown>();
		}
	}
}
