using System;

namespace BatchLib.ViewModels.ConsoleModel
{
	public class AuditLog
	{
		public long id
		{
			get;
			set;
		}

		public long? CustomerId
		{
			get;
			set;
		}

		public long? GoalId
		{
			get;
			set;
		}

		public string ActionDesc
		{
			get;
			set;
		}

		public DateTime Created
		{
			get;
			set;
		}

		public string CreatedBy
		{
			get;
			set;
		}

		public string Source
		{
			get;
			set;
		}
	}
}
