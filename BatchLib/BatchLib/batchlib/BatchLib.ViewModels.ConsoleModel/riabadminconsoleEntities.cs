using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace BatchLib.ViewModels.ConsoleModel
{
	public class riabadminconsoleEntities:DbContext
	{
		public virtual DbSet AuditLogs
		{
			get;
			set;
		}

		public riabadminconsoleEntities()
			: base("name=riabadminconsoleEntities")
		{
		}
        
		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			//IL_0001: Unknown result type (might be due to invalid IL or missing references)
			throw new UnintentionalCodeFirstException();
		}
        
	}
}
